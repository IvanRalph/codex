const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
    resolve : {
        extensions : [
            '.js',
            '.vue'
        ],
        alias : {
            '@' : __dirname + '/resources/js'
        }
    }
});

mix.autoload({
    'jquery': ['$', 'window.jQuery', 'jQuery'],
    'vue': ['Vue','window.Vue'],
    'moment': ['moment','window.moment'],
})
;

mix.js('resources/js/main.js', 'public/js')
    .js('resources/js/login.js', 'public/js')
    .js('resources/metronic/assets/js/scripts.bundle.js', 'public/js/scripts.js')
    .sass('resources/sass/app.scss', 'public/css/app.css')
    .sass('resources/css/login.scss', 'public/css/login.css')
    .styles([
        'resources/metronic/assets/css/flaticon.css',
        'resources/metronic/assets/css/flaticon2.css',
        'resources/metronic/assets/css/style.bundle.css',
    ], 'public/css/style.css')
    .copy('resources/metronic/assets/media/bg-1.jpg', 'public/images/bg-1.jpg')
    .copy('resources/metronic/assets/media/logo-8.png', 'public/images/logo-8.png')
    .copy('resources/metronic/assets/media/logo-8-inverse.png', 'public/images/logo-8-inverse.png')
    .copy('resources/css/font', 'public/css/font');
