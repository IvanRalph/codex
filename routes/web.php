<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\mClass;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::group([
    'namespace'  => 'App\Http\Controllers\api',
    'middleware' => 'auth',
    'prefix'     => 'api'
], function () {
    Route::resource('case-types', 'CaseTypeController');

    Route::resource('service-lines', 'ServiceLineController');

    Route::resource('systems', 'SystemController');

    Route::resource('cases', 'CaseMaintenanceApiController');

    Route::resource('classes', 'ClassController');

    Route::resource('users', 'UserController');

    Route::resource('class-pax', 'ClassPaxController');

    Route::resource('roles', 'RoleController');

    Route::resource('permissions', 'PermissionController');

    Route::resource('roles-permission', 'RolePermissionController');

    Route::resource('assignments', 'AssignmentController');

    Route::resource('code-references', 'ReferenceController');

    Route::delete('class-pax', 'ClassPaxController@bulkDelete');

    Route::delete('cases', 'CaseMaintenanceApiController@bulkDelete');

    Route::delete('classes', 'ClassController@bulkDelete');

    Route::delete('assignments', 'AssignmentController@bulkDelete');

    Route::get('cases/export/pdf/{case_id}', 'CaseMaintenanceApiController@exportPdf');

    Route::get('cases/download/pdf/{case_id}', 'CaseMaintenanceApiController@downloadPdf');

    Route::get('cases/export/csv', 'CaseMaintenanceApiController@ExportCSV');

    Route::get('cases/fetch/{field_name}/{caseTypeId?}/{serviceLineId?}/{system?}', 'CaseMaintenanceApiController@getByField');

    Route::get('cases/handling-time/{case_id}/{class_id}', 'CaseMaintenanceApiController@handlingTime');

    Route::get('classes/export/csv', 'ClassController@ExportCSV');

    Route::get('classes/export/pdf/{class_id}', 'ClassController@exportPdf');

    Route::get('/authenticated-user/classes', 'ClassController@getClassByAuthUser');

    Route::get('/authenticated-user', 'UserController@authUser');

    Route::get('training-module/cases/{class_id}', 'TrainingModuleController@index');

    Route::post('training-module/pause/{case_id}/{class_id}', 'TrainingModuleController@pause');

    Route::post('training-module/stop/{case_id}/{class_id}', 'TrainingModuleController@complete');

    Route::post('training-module/save/{case_id}/{class_id}', 'TrainingModuleController@save');

    Route::get('training-module/get-previous-answer/{case_id}/{class_id}', 'TrainingModuleController@getPreviousAnswer');

    Route::get('training-module/get-saved-answer/{case_id}/{class_id}', 'TrainingModuleController@getSavedAnswer');

    Route::get('my-privileges', 'UserController@getPermissions');
    
});

Route::group(['namespace' => 'Modules\Login\Http\Controllers'], function () {
    Route::get('login/microsoft', 'web\LoginController@redirectToProvider')->name('login');

    Route::get('login/microsoft/callback', 'web\LoginController@handleProviderCallback');
});

Route::group(['namespace' => 'App\Http\Controllers'], function () {
    Route::get('/login', 'LoginController@index');

    Route::get('/{any}', 'SpaController@index')->where('any', '.*');
});
