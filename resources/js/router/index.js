import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/pages/home/index';
import CaseMaintenance from '../components/pages/CaseMaintenance/CaseMaintenance';
import ClassManagement from '../components/pages/Class Management/ClassManagement';
import ClassForm from '../components/pages/Class Management/form';
import ClassIndex from '../components/pages/Class Management/index';
import CaseIndex from '../components/pages/CaseMaintenance/index';
import CaseForm from '../components/pages/CaseMaintenance/form';
import TrainingModule from '../components/pages/Training Module/TrainingModule';
import TrainingIndex from '../components/pages/Training Module/index';
import TrainingForm from '../components/pages/Training Module/form';
import SettingsModule from '../components/pages/settings/SettingsModule';
import RolesModule from '../components/pages/settings/roles/RolesModule';
import UsersModule from '../components/pages/settings/users/UsersModule';
import ReferencesModule from '../components/pages/settings/references/ReferencesModule';
import CaseTypesModule from '../components/pages/settings/references/case types/CaseTypesModule';
import ServiceLinesModule from '../components/pages/settings/references/service lines/ServiceLinesModule';
import SystemsModule from '../components/pages/settings/references/systems/SystemsModule';
import CasesModule from '../components/pages/Class Management/tabs/cases';
import TraineesModule from '../components/pages/Class Management/tabs/trainees';
import PageNotFound from '../components/pages/404';
import NoAccess from '../components/pages/misc/NoAccess';
import TextUploader from '../components/pages/settings/references/ref_code/reference';

Vue.use(Router);

export default new Router({
    linkActiveClass : 'active',
    mode : 'history',
    routes : [
        {
            path : '/',
            redirect : '/case-maintenance/index'
        },
        {
            path : '/case-maintenance',
            name : 'CaseMaintenance',
            redirect : '/case-maintenance/index',
            component : CaseMaintenance,
            children : [
                {
                    path : 'index',
                    component : CaseIndex
                },
                {
                    path : 'create',
                    component : CaseForm
                },
                {
                    path : 'edit/:caseid',
                    component : CaseForm
                }
            ]
        },
        {
            path : '/class-management',
            redirect : '/class-management/index',
            component : ClassManagement,
            children : [
                {
                    path : 'create',
                    name : 'ClassInformation',
                    component : ClassForm,
                },
                {
                    path : 'index',
                    component : ClassIndex,
                },
                {
                    path : 'edit/:classid',
                    component : ClassForm,
                    redirect : '/class-management/edit/:classid/cases',
                    children : [
                        {
                            path : 'cases',
                            name : 'cases',
                            component : CasesModule,
                        },
                        {
                            path : 'trainees',
                            name : 'trainees',
                            component : TraineesModule,
                        },
                    ]
                }
            ]
        },
        {
            path : '/training-module',
            redirect : '/training-module/index',
            component : TrainingModule,
            children : [
                {
                    path : 'index',
                    component : TrainingIndex,
                },
                {
                    path : 'case/:caseid/:classid',
                    component : TrainingForm,
                }
            ]
        },
        {
            path : '/settings',
            redirect : '/settings/roles',
            component : SettingsModule,
            children : [
                {
                    path : 'roles',
                    component : RolesModule,
                },
                {
                    path : 'users',
                    component : UsersModule,
                },
                {
                    path : 'references',
                    component : ReferencesModule,
                    redirect : '/settings/references/case-types',
                    children : [
                        {
                            path : 'case-types',
                            component : CaseTypesModule,
                        },
                        {
                            path : 'service-lines',
                            component : ServiceLinesModule,
                        },
                        {
                            path : 'systems',
                            component : SystemsModule,
                        },
                        {
                            path : 'text-upload',
                            component : TextUploader
                        },
                    ]
                }
            ]
        },
        {
            path : '/no-access',
            component : NoAccess
        },
        {
            path : '*',
            component : PageNotFound
        }
    ]
})

