// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
require('bootstrap');
window.DataTable = require('datatables.net-bs4');
window.Swal = require('sweetalert2');
window.Moment = require('moment');
window.Sticky = require('sticky-js');

import Vue from 'vue'
import App from './App';
import router from './router'
import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import "timecircles/inc/TimeCircles.js";
import "timecircles/inc/TimeCircles.css";
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css";
import Axios from 'axios';
import vSelect from 'vue-select';
import 'vue-select/dist/vue-select.css';
import BlockUI from 'vue-blockui'

Vue.use(datePicker);
Vue.use(BlockUI);
Vue.component('v-select', vSelect);

window.Toast = Swal.mixin({
    toast : true,
    position : 'top-end',
    showConfirmButton : false,
    timer : 6000
});

window._toastr = function (type, msg) {
    Toast.fire({
        type : type,
        title : msg
    })
};

//GLOBAL API CALL ERROR HANDLER
Axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 401) {
        _sessionExpiredAlert();
    } else if (error.response.status === 403){
        _toastr('error', 'Access Denied');
    } else {
        _toastr('error', 'Something went wrong..');
    }

    return Promise.reject(error);
});

// DATATABLE ERROR HANDLER
$.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) {
    if (settings.jqXHR.status === 401) {
        _sessionExpiredAlert();
    } else if (settings.jqXHR.status === 403){
        _toastr('error', 'Access Denied');
    }  else {
        _toastr('error', 'Something went wrong..');
    }
};

window._sessionExpiredAlert = function () {
    Swal.fire({
        title : 'Oops..',
        text :
            "Session has expired. Please refresh the page..",
        type : 'warning',
        showCancelButton : false,
        confirmButtonText :
            'Refresh',
        animation : false,
        allowOutsideClick : false,
        allowEscapeKey : false,
    }).then((value) => {
        if (value) {
            window.location.reload();
        }
    });
};

window._perms = [];

//Global Methods
Vue.mixin({
    data() {
        return {
            today : Moment(new Date()).format('YYYY-MM-DD'),

            user : 'User Name',

            errors : [],

            showGlobalLoader : true,

            showButtonLoader : false,

            showModalLoader : true,

            privileges : [],

            references : [],

            datepicker : {
                options : {
                    widgetPositioning : {
                        horizontal : 'left',
                        vertical : 'top'
                    }
                }
            },
        }
    },

    methods : {
        $_hasAccess(name) {
            var x;
            $.each(_perms, function (key, value) {
                if (value.name === name) {
                    x = value.has_access === 1;
                    return false;
                }
            });

            return x;
        },

        $_getPermissions() {
            let vm = this;
            return Axios.get('/api/my-privileges')
                .then((response) => {
                    _perms = response.data;
                });
        },

        $_logout() {
            Axios.post('/api/logout').then(function (response) {
                var data = response.data;

                window.location.href =
                    data.redirect_url;
            })
        },

        $_getAuthenticatedUser() {
            var self = this;
            Axios.get('/api/authenticated-user')
                .then(function (response) {
                    self.user = response.data;
                });
        },

        $_displayValidationError(errors) {
            this.errors = errors;
            // $('.is-invalid').removeClass('is-invalid');
            // $('.alert').addClass('kt-hidden');
            // $.each(errors, function (key, value) {
            //     $('#' + key +
            //         ', .' + key).addClass('is-invalid');
            //
            //     $('#' + key + '_feedback').html(value);
            //
            //     if (key == 'SDPDX_dup_codes' || key == 'dup_codes_spx' || key == 'dup_codes_ppx' || key ==
            // 'PPXSPX_dup_codes') { $('.' + key).removeClass('kt-hidden'); } });
        },

        $_resetInitialData() {
            Object.assign(this.$data,
                this.$options.data());
        },

        $_resetForm() {
            $('form').trigger('reset');
        },

        $_resetFields() {
            Object.assign(this.$data.fields,
                this.$options.data().fields);
        },

        $_exportFile(url, fileName) {
            $('body').css('cursor', 'progress');
            return Axios.get(url, {
                responseType : 'blob'
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download',
                    fileName);
                document.body.appendChild(link);
                link.click();
                link.parentNode.removeChild(link);
                this.showButtonLoader = false;

                $('body').css('cursor', 'default');
            });
        },

        // DEPRECATED FUNCTION
        // $_discardForm(redirectUrl = null) {
        //     Swal.fire({
        //         title : 'Are you sure?',
        //         text : "You won't be able to revert this!",
        //         type : 'warning',
        //         showCancelButton : true,
        //         confirmButtonText : 'Yes, discard form',
        //         cancelButtonText : 'No'
        //     }).then((result) => {
        //         if (result.value) {
        //             if (redirectUrl) {
        //                 this.$router.go(-1);
        //             } else {
        //                 this.$router.push(redirectUrl);
        //             }
        //         } else {
        //             this.showButtonLoader = false;
        //         }
        //     })
        // },

        $_removeRow(event) {
            event.target.closest('tr').remove();
        },

        $_deleteRow(url, table) {
            var ids = [];
            $('.dataCheckBox:checkbox:checked').each(function (key, value) {
                ids.push($(value).data('id'));
            });

            if (ids.length > 0) {
                let multipleRowsMsg = ids.length > 1 ? "<span class='font-weight-bolder text-danger'>Selected Rows: " + ids.length + "</span><br>" : "";
                this.showButtonLoader = true;
                Swal.fire({
                    title : 'Are you sure?',
                    html : multipleRowsMsg + "You won't be able to revert this!",
                    type : 'question',
                    showCancelButton : true,
                    confirmButtonText : 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        Axios.delete(url, {
                            data :
                                {ids : ids}
                        }).then((response) => {
                            table.ajax.reload();
                            this.showButtonLoader = false;
                            setTimeout(() => {
                                this.$router.go();
                            },1500);
                            _toastr('success',
                                response.data.message);

                        }).catch((error) => {
                            this.showButtonLoader =
                                false;
                            _toastr('error',
                                error.response.data.message);
                        });
                    }
                })
            }

            this.showButtonLoader = false;
        },

        $_tableRowClickableInit(redirectUrl, tableId) {
            var self = this;
            $(tableId).on('click',
                'tr > td:not(.checkBox)',
                function
                    () {
                    self.$router.push(redirectUrl +
                        $(this).siblings('td').eq(1).html());
                });
        },

        $_selectAllCheckbox() {
            if ($('#selectAll, #selectAll2').is(':checked')) {
                $('.dataCheckBox').prop('checked',
                    true);
            } else {
                $('.dataCheckBox').prop('checked', false);
            }
        },

        $_searchTable(table, search) {
            table.search(search).draw();
        },

        $_countPax(){
            this.$root.$emit('count-pax');
        },

        $_getCodeReferences() {
            return Axios.get('/api/code-references')
                .then(function (response) {
                    this.references = response;
                });
        }
    }
});

/* eslint-disable no-new */
new Vue({
    el : '#app',
    router,
    components : {App}
});

//DateTime picker icons
jQuery.extend(true, jQuery.fn.datetimepicker.defaults, {
    icons : {
        time : 'flaticon-clock-1',
        date : 'flaticon-calendar-1',
        up : 'flaticon2-up',
        down : 'flaticon2-down',
        previous : 'flaticon2-back',
        next : 'flaticon2-next',
        today : 'flaticon-calendar-with-a-clock-time-tools',
        clear : 'flaticon2-trash',
        close : 'flaticon-cancel'
    }
});

(function ($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
        $("body").toggleClass("sidebar-toggled");
        $(".sidebar").toggleClass("toggled");
        if ($(".sidebar").hasClass("toggled")) {
            $('.sidebar .collapse').collapse('hide');
        }
        ;
    });

    // Close any open menu accordions when window is resized below 768px
    $(window).resize(function () {
        if ($(window).width() < 768) {
            $('.sidebar .collapse').collapse('hide');
        }
        ;
    });

    // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
        if ($(window).width() > 768) {
            var e0 = e.originalEvent,
                delta = e0.wheelDelta || -e0.detail;
            this.scrollTop += (delta < 0 ? 1 : -1) * 30;
            e.preventDefault();
        }
    });

    // Scroll to top button appear
    $(document).on('scroll', function () {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 100) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function (e) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop : ($($anchor.attr('href')).offset().top)
        }, 1000, 'easeInOutExpo');
        e.preventDefault();
    });

})(jQuery); // End of use strict