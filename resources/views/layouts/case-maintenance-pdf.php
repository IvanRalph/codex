<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

</head>
<body>
<table class="table table-bordered">
    <tr>
        <td>
            {{$classDetails->description}}
        </td>
        <td>
            {{$classDetails->target_end}}
        </td>
        <td>
            {{$classDetails->no_of_pax}}
        </td>
        <td>
            {{$classDetails->show_score}}
        </td>
        <td>
            {{$classDetails->show_answer}}
        </td>
        <td>
            {{$classDetails->show_rationale}}
        </td>
        <td>
            {{$classDetails->show_prev_ans}}
        </td>
    </tr>

</table>
</body>
</html>
