<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{--<link href="{{ mix('css/style.css') }}" rel="stylesheet" type="text/css"/>--}}
    <style>
        .no-border {
            border: none !important;
        }

        .kt-font-primary {
            color: #5867dd !important;
        }

        .table .thead-purple th {
            color: #fff;
            background-color: #5867dd;
            border-color: #3f9ae5;
            padding: 1px !important;
        }

        td {
            padding: 2px !important;
            font-size: 15px;
        }

        label {
            display: inline-block !important;
            font-size: 15px;
        }

        .center {

            margin-left: 18.5rem;
            opacity: 0.7;
        }

        .div-block {
            width: 50%;
            float: left;

        }

        small {
            font-size: 8px;
        }

        div.footer {
            position: fixed;
            bottom: 10px;

        }

    </style>
</head>
<body>
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <img class="center" src="{{asset('/images/codex-logo1.png')}}" height="48px" width="136px">
            <h5 class="text-center text-muted ">
                CODE # {{$classDetails->id}}
            </h5>
            <hr>
            <table>
                <tbody>
                <tr>
                    <td class="align-top"><b>Creator:</b></td>
                    <td class="align-top">{{$classDetails->created_by}}</td>
                    <td class="align-top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Created On:</b>
                    </td>
                    <td class="align-top">{{$classDetails->created_at}}</td>

                </tr>
                <tr>
                    <td class="align-top"><b>No. Of Pax:</b></td>
                    <td class="align-top">{{$classDetails->no_of_pax}}</td>
                    <td class="align-top">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Description:</b></td>
                    <td class="align-top">{{$classDetails->description}}</td>

                </tr>
                <tr>
                    <td class="align-top"><b>Completed:</b></td>
                    <td class="align-top">{{ (int) (($classDetails->completed <= 100)?($classDetails->completed):'100') }}%</td>
                    <td class="align-top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Target
                                                                                                         Completion:</b>
                    </td>
                    <td class="align-top">{{$classDetails->target_end}}</td>
                </tr>

                <tr>
                    <td class="align-top"><b>Passing Rate: </b></td>
                    <td class="align-top">{{$classDetails->passing_rate ?? '0'}}%</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
    <hr>

    <br><br>

    <div class="row">
        <div class="col-md-12">
            <h6 class="text-center kt-font-primary">
                CASES
            </h6>
        </div>
    </div>
    <br><br>

    <div class="row" id="cases">

        <table id="pdx" class="table table-bordered  ">
            <thead class="thead-purple">
            <tr>
                <th class="text-center">CASE TYPE</th>
                <th class="text-center">SERVICE LINE</th>
                <th class="text-center">SYSTEM</th>
                <th class="text-center">DESCRIPTION</th>
                <th class="text-center">% COMPLETED</th>
                <th class="text-center">AVG SCORE</th>
                <th class="text-center">PASSED</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cases as $cases):
            <tr>
                <td>{{$cases->case_type_id}}</td>
                <td>{{$cases->service_line_id}}</td>
                <td>{{$cases->system}}</td>
                <td>{{$cases->description}}</td>
                <td>{{$cases->completed_pct}}</td>
                <td>{{$cases->avg_score}}</td>
                <td>{{($cases->avg_score > 75)? 'Passed' : 'Failed'}}</td>
            </tr>
            @endforeach

            </tbody>
        </table>

    </div>
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <h6 class="text-center kt-font-primary">
                TRAINEES
            </h6>
        </div>
    </div>
    <br><br>

    <div class="row" id="trainees">
        <table id="pdx" class="table table-bordered  ">
            <thead class="thead-purple">
            <tr>
                <th class="text-center">NAME</th>
                <th class="text-center">COMPLETED</th>
                <th class="text-center">IN PROGRESS</th>
                <th class="text-center">NOT STARTED</th>
                <th class="text-center">AVG SCORE</th>
                <th class="text-center">POA SCORE</th>
            </tr>
            </thead>
            <tbody>
            @foreach($trainees as $trainees):
            <tr>
                <td>{{$trainees->user}}</td>
                <td>{{$trainees->completed}}</td>
                <td>{{$trainees->in_progress}}</td>
                <td>{{$trainees->not_started}}</td>
                <td>{{$trainees->avg_score}}</td>
                <td>{{$trainees->poa_score}}</td>

            </tr>
            @endforeach

            </tbody>
        </table>

    </div>

    <br><br><br><br><br><br><br>
    <div class="footer">
        <small class="text-muted">
            CODEX - 2019
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;

            PDF GENERATED ON : {{NOW()}}
        </small>
    </div>
</div>
</div>
</body>
</html>