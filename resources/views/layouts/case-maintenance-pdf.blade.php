<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    {{--<link href="{{ mix('css/style.css') }}" rel="stylesheet" type="text/css"/>--}}
    <style>
        table{
            width:100% !important;
        }
        .no-border {
            border: none !important;
        }

        .kt-font-primary {
            color: #5867dd !important;
        }

        .table .thead-purple th{
            color: #fff !important;
            background-color: #5867dd !important;
            border-color: #3f9ae5 !important;
            padding: 1px !important;
        }
        td {
            padding: 2px !important;
            font-size: 14px;
        }

        label {
            display: inline-block !important;
            font-size: 15px;
        }
        .center {
            margin-left: 18.5rem;
            opacity: 0.7;
        }
        .div-block {
            width:50%;
            float:left;

        }
        small {
            font-size: 8px;
        }
        .page-break{
            page-break-after: always;
        }
        div.footer {
            position: fixed;
            bottom: 10px;

        }
        .align-top {
            vertical-align:top;
        }

        .text-center{
            text-align: center;
        }

        .text-left{
            text-align: left;
        }

        .text-right{
            text-align: right;
        }

    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <img class="center" src="{{asset('/images/codex-logo1.png')}}" height="48px" width="136px">
            <h5 class="text-center text-muted ">
                CASE # {{$caseDetails->account}}
            </h5>
            <hr>
            <table>
                <tbody>
                <tr>
                    <td class="align-top"><b>Case Type:</b></td>
                    <td class="align-top">{{$otherDetails['caseType']}}</td>
                    <td class="align-top"><b>Account:</b></td>
                    <td class="align-top">{{$caseDetails->account}}</td>
                </tr>
                <tr>
                    <td class="align-top"><b>Service line:</b></td>
                    <td class="align-top">{{$otherDetails['serviceLine']}}</td>
                    <td class="align-top"><b style="font-size: 13px;">File Name:</b></td>
                    <td class="align-top">{{$caseDetails->file}}</td>
                </tr>
                <tr>
                    <td class="align-top"><b>System:</b></td>
                    <td class="align-top">{{$otherDetails['systems']}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                <tr>
                    <td class="align-top"><b>Facility:</b></td>
                    <td class="align-top">{{$caseDetails->facility?$caseDetails->facility:'n/a'}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                <tr>
                    <td class="align-top"><b>Patient:</b></td>
                    <td class="align-top">{{$caseDetails->patient?$caseDetails->patient:'n/a'}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                <tr>
                    <td class="align-top"><b>Admission:</b></td>
                    <td class="align-top">{{$caseDetails->admission?$caseDetails->admission:'n/a'}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                <tr>
                    <td class="align-top"><b>Discharge:</b></td>
                    <td class="align-top">{{$caseDetails->discharge?$caseDetails->discharge:'n/a'}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                <tr>
                    <td class="align-top"><b>Description:</b></td>
                    <td class="align-top">{{$caseDetails->description}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                 <tr>
                    <td class="align-top"><b>Rationale:</b></td>
                    <td class="align-top">{{$caseDetails->rationale}}</td>
                    <td class="align-top"></td>
                    <td class="align-top"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <hr>

    <br><br>

    <div class="row">
        <div class="col-md-12">
            <h3 class="text-center kt-font-primary">
                DIAGNOSIS
            </h3>
        </div>
    </div>
    <br><br>

    <div class="row" id ="diagnosis">
            <table class="table" style="width:100%" border="1">
                    <thead class="thead-purple">
                     <tr>
                         <th class="text-center" colspan="2">PDX</th>
                        <th class="text-center" colspan="2">PDX</th>
                     </tr>
                     <tr>
                        <th class="text-center">POA</th>
                        <th class="text-center">CODE</th>
                        <th class="text-center">POA</th>
                        <th class="text-center">CODE</th>
                     </tr>
                    </thead>
                    <tbody>
                        @php
                            if(count($keyset["pdx"]) > count($keyset["sdx"])){
                                $mostGreaterD = count($keyset["pdx"]);
                            }else{
                                $mostGreaterD = count($keyset["sdx"]);
                            }
                            for ($d = 0; $d < $mostGreaterD; $d++){
                                echo '<tr>
                                    <td class="text-center" style="width:50%">'. @$keyset["pdx"][$d]["poa"].'</td>
                                    <td class="text-center" style="width:50%">'. @$keyset["pdx"][$d]["code"].'</td>
                                    <td class="text-center" style="width:50%">'. @$keyset["sdx"][$d]["poa"].'</td>
                                    <td class="text-center" style="width:50%">'. @$keyset["sdx"][$d]["code"].'</td>
                                </tr>';
                            }
                        @endphp
                    </tbody>
            </table>
    </div>
    <br><br>
    <div class="page-break"></div>
    <div class="row" id ="title-procedure">
        <div class="col-md-12">
            <h3 class="text-center kt-font-primary">
                PROCEDURE
            </h3>
        </div>
    </div>
    <br><br>


    <div class="row" id ="procedure">

            <table class="table" style="width:100%" border="1">
                <thead class="thead-purple">
                    <tr>
                        <th class="text-center" colspan="2">PPX</th>
                    </tr>
                    <tr>
                        <th class="text-center">PPX</th>
                        <th class="text-center">SPX</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        if(count($keyset["ppx"]) > count($keyset["spx"])){
                            $mostGreaterP = count($keyset["ppx"]);
                        }else{
                            $mostGreaterP = count($keyset["spx"]);
                        }
                        for ($p = 0; $p < $mostGreaterP; $p++){
                            echo '<tr>
                                <td class="text-center" style="width:50%">'.@$keyset["ppx"][$p].'</td>
                                <td class="text-center" style="width:50%">'.@$keyset["spx"][$p].'</td>
                            </tr>';
                        }
                    @endphp
                </tbody>
            </table>
    </div>


    <br><br><br><br>
    <br><br><br><br>
    <div class="footer">
        <small class="text-muted">
            <table class="table table-bordered">
                <tr>
                    <td class="text-left"><small>CODEX - {{ date('Y') }}</small></td>
                    <td class="text-right"><small>PDF GENERATED ON : {{NOW()}}</small></td>
                </tr>
            </table>
        </small>
    </div>
</div>

</body>
</html>