<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ mix('css/login.css') }}">
</head>
<body class="my-login-page">
<section class="h-100">
    <div id="app">
        <login-component login-parameter-label="{{ config('login.login_parameter_label') }}" login-parameter="{{ config('login.login_parameter') }}" login-remember-me="{{ config('login.remember_me') }}"
        ></login-component>
    </div>
</section>

{{--<script src="{{ mix('js/main.js') }}"></script>--}}
<script src="{{ mix('js/login.js') }}"></script>
</body>
</html>