<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class mCase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'description',
        'file',
        'case_type_id',
        'service_line_id',
        'system',
        'facility',
        'account',
        'patient',
        'discharge',
        'admission',
        'keyset',
        'rationale',
        'status',
        'updated_by',
        'is_practice_case'
    ];

    protected $table = 'cases';

    protected $casts = [
        'keyset' => 'array'
    ];

    //relationship
    public function caseType() {
        return $this->hasOne('App\CaseType', 'id', 'case_type_id');
    }

    public function serviceLine() {
        return $this->hasOne('App\ServiceLine', 'id', 'service_line_id');
    }

    public function systems() {
        return $this->hasOne('App\System', 'id', 'system');
    }

    public function user ()
    {
        return $this->hasOne('App\User', 'id', 'updated_by');
    }

    public static function workQueueTable ($classId)
    {
        $cases = mCase::hydrate(DB::select('CALL get_tbl_training('. auth()->user()->id .', '. $classId .')'));

        return datatables($cases)
            ->toJson();
    }

    public static function caseMaintenanceTable ()
    {
        $cases = mCase::join('case_types', 'cases.case_type_id', 'case_types.id')
            ->join('service_lines', 'cases.service_line_id', 'service_lines.id')
            ->join('systems', 'cases.system', 'systems.id')
            ->leftJoin('users', 'cases.updated_by', 'users.id')
            ->select('cases.id', 'case_types.description AS case_type_id',
                'service_lines.description AS service_line_id', 'systems.description AS system',
                'cases.description AS description', 'cases.facility', 'cases.updated_at',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) AS updated_by'), 'cases.file');

        return $cases;
    }
}

