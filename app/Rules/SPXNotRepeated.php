<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class SPXNotRepeated implements Rule
{
    protected $spx;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($spx)
    {
        $this->spx = $spx;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $index = explode('.', $attribute)[2];

        Arr::forget($this->spx, $index);

        return !in_array($value, $this->spx);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The SPX Code field must not be repeated.';
    }
}
