<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class POARequiredIfSDX implements Rule
{
    protected $pdx;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($pdx)
    {
        $this->pdx = $pdx;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $index = explode('.', $attribute)[2];

        if ((!$value && !$this->pdx[$index]) || ($value && $this->pdx[$index]) || ($value && !$this->pdx[$index])) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The SDX POA Field is required if SDX Code is present.';
    }
}
