<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class SDXNotRepeated implements Rule
{
    protected $sdx;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($sdx)
    {
        $this->sdx = $sdx;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $index = explode('.', $attribute)[2];

        Arr::forget($this->sdx, $index);

        return !in_array($value, $this->sdx);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The SDX Code must not be repeated.';
    }
}
