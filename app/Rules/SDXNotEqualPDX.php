<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SDXNotEqualPDX implements Rule
{
    protected $pdx;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($pdx)
    {
        $this->pdx = $pdx;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !in_array($value, $this->pdx);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'SDX Code should not have any duplicate in PDX Code.';
    }
}
