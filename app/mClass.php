<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mClass extends Model
{
    protected $table = 'classes';

    protected $fillable
        = [
            'description',
            'target_end',
            'created_by',
            'no_of_pax',
            'show_score',
            'show_answer',
            'show_rationale',
            'show_prev_ans',
            'passing_rate'
        ];

    //relationship
    public function assignments() {
        return $this->hasMany("App\Assignment", "class_id", "id");
    }

    public function classpax() {
        return $this->hasMany("App\ClassPax", "class_id", "id");
    }
}
