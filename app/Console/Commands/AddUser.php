<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class AddUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add user to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle ()
    {
        $fname    = $this->ask('First Name: ');
        $mname    = $this->ask('Middle Name: ');
        $lname    = $this->ask('Last Name: ');
        $username = $this->ask('NT Login: ');

        User::insert([
            'first_name'  => $fname,
            'middle_name' => $mname,
            'last_name'   => $lname,
            'username'    => $username
        ]);

        $this->info('User created successfully.');
    }
}
