<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assignment extends Model
{
    use SoftDeletes;

    protected $table = 'assignments';

    protected $fillable
        = [
            'class_id',
            'case_id',
            'date_assigned',
            'assigned_by'
        ];

}
