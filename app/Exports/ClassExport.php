<?php
/**
 * Created by PhpStorm.
 * User: ecvitug
 * Date: 5/20/2019
 * Time: 11:38 PM
 */

namespace App\Exports;

use App\Http\Repository\ClassRepository;
use App\mClass;
use Maatwebsite\Excel\Concerns\FromCollection;

class ClassExport implements FromCollection
{
    protected $classRepository;

    public function __construct (ClassRepository $classRepository)
    {
        $this->classRepository = $classRepository;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection ()
    {
        $headerArr = [
            'Class Code',
            'Description',
            'No_of_Fax',
            'End',
            'Created_by',
            'Created_on'
        ];

        $data = collect();
        $data->push($headerArr);
        $data->push($this->classRepository->getTableData()->get());

        return $data;
    }
}
