<?php

namespace App\Exports;

use App\Http\Repository\CaseRepository;
use App\mCase;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;

class CaseExport implements FromCollection
{
    use Exportable;

    protected $caseRepository;

    public function __construct (CaseRepository $caseRepository)
    {
        $this->caseRepository = $caseRepository;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection ()
    {
        $headerArr = [
            'id',
            'Case Type',
            'Service Line',
            'System',
            'Description',
            'Facility',
            'Last Updated',
            'Updated By',
            'File'
        ];

        $data = collect();
        $data->push($headerArr);
        $data->push($this->caseRepository->getTableData()->get());

        return $data;
    }
}
