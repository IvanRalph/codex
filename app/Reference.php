<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reference extends Model
{
    protected $table = 'reference';

    protected $fillable = [
        'modified_by',
        'updated_at'
    ];
}
