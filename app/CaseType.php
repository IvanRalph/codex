<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CaseType extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'description',
        'with_poa',
        'created_by',
        'updated_by'
    ];
}
