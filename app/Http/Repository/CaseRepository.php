<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 4/29/2019
 * Time: 8:26 PM
 */

namespace App\Http\Repository;

use App\CaseType;
use App\mCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class CaseRepository
{
    public function getTableData ($isTrainingModule = false, $classId = null)
    {
        if ($isTrainingModule) {
            return mCase::workQueueTable($classId);
        }

        return mCase::caseMaintenanceTable();
    }

    public function validatePoa ($request)
    {
        $poaList = ['N', 'Y', 'E', 'U', 'W'];

        $errors = array(
            'message' => 'The given data was invalid.'
        );

        $caseType = CaseType::find($request['case_type_id']);

        if ($caseType->with_poa == 0 || $caseType->with_poa == 1) {
            if($caseType->with_poa == 1){
                if (!isset($request['pdx_poa'])) {
                    $errors['errors']['dup_codes_pdx'] = 'PDX POA fields are required';
                } else {
                    foreach ($request['pdx_poa'] as $pdxKey => $pdxValue) {
                        if ($pdxValue == "") {
                            $errors['errors']['pdx_poa'][$pdxKey] = 'PDX POA fields are required';
                        } else {
                            if (!in_array(strtoupper($pdxValue), $poaList)) {
                                $errors['errors']['pdx_poa'][$pdxKey] = 'PDX POA field only accept "Y", "N", "E", "U" and "W"';
                            }
                        }
                    }
                }
            }

            if (!isset($request['pdx_code'])) {
                $errors['errors']['pdx_code'] = 'PDX CODE fields are required';
            } else {
                $duplicateResPdx = array_unique(array_diff_assoc($request['pdx_code'],
                    array_unique($request['pdx_code'])));
                if (count($duplicateResPdx) > 0) {
                    $errors['errors']['dup_codes_pdx'] = 'Encountered Duplicate in PDX';
                }
                foreach ($request['pdx_code'] as $pdxCodeKey => $pdxCodeValue) {
                    if ($pdxCodeValue == "") {
                        $errors['errors']['pdx_code'][$pdxCodeKey] = 'PDX CODE fields are required';
                    }else if (strlen($pdxCodeValue) < 3 || strlen($pdxCodeValue) > 8) {
                        $errors['errors']['pdx_code'][$pdxCodeKey] = 'PDX CODE field only accept min of 3 and max of 8 characters';
                    }
                }
            }

            if($caseType->with_poa == 1){
                if (!isset($request['sdx_poa'])) {
                    $errors['errors']['dup_codes_sdx'] = 'SDX POA fields are required';
                } else {
                    foreach ($request['sdx_poa'] as $sdxKey => $sdxValue) {
                        if ($sdxValue == "") {
                            $errors['errors']['sdx_poa'][$sdxKey] = 'SDX POA fields are required';
                        } else {
                            if (!in_array(strtoupper($pdxValue), $poaList)) {
                                $errors['errors']['pdx_poa'][$pdxKey] = 'PDX POA field only accept "Y", "N", "E", "U" and "W"';
                            }
                        }
                    }
                }
            }

            if (!isset($request['sdx_code'])) {
                $errors['errors']['dup_codes_sdx'] = 'SDX CODE fields are required';
            } else {
                $duplicateResSdx = array_unique(array_diff_assoc($request['sdx_code'],
                    array_unique($request['sdx_code'])));
                if (count($duplicateResSdx) > 0) {
                    $errors['errors']['dup_codes_sdx'] = 'Encountered Duplicate in SDX';
                }
                foreach ($request['sdx_code'] as $sdxCodeKey => $sdxCodeValue) {
                    if ($sdxCodeValue == "") {
                        $errors['errors']['sdx_code'][$sdxCodeKey] = 'SDX CODE fields are required';
                    }else if (strlen($sdxCodeValue) < 3 || strlen($sdxCodeValue) > 8) {
                        $errors['errors']['sdx_code'][$sdxCodeKey] = 'SDX CODE field only accept min of 3 and max of 8 characters';
                    }
                }
            }

            foreach ($request['ppx_code'] as $key => $value) {
                if ($value == "") {
                    unset($request['ppx_code'][$key]);
                }
            }
            foreach ($request['spx_code'] as $key => $value) {
                if ($value == "") {
                    unset($request['spx_code'][$key]);
                }
            }
            foreach ($request['pdx_code'] as $key => $value) {
                if ($value == "") {
                    unset($request['pdx_code'][$key]);
                }
            }
            foreach ($request['sdx_code'] as $key => $value) {
                if ($value == "") {
                    unset($request['sdx_code'][$key]);
                }
            }

            if (!empty($request['ppx_code'])) {
                $duplicateResPpx = array_unique(array_diff_assoc($request['ppx_code'],
                    array_unique($request['ppx_code'])));
                if (count($duplicateResPpx) > 0) {
                    $errors['errors']['dup_codes_ppx'] = 'Encountered Duplicate in PPX';
                }
                foreach ($request['ppx_code'] as $ppxCodeKey => $ppxCodeValue) {
                    if (strlen($ppxCodeValue) < 3 || strlen($ppxCodeValue) > 8) {
                        $errors['errors']['ppx_code'][$ppxCodeKey] = 'PPX CODE field only accept min of 3 and max of 8 characters';
                    }
                }
            }

            if (!empty($request['spx_code'])) {
                $duplicateResSpx = array_unique(array_diff_assoc($request['spx_code'],
                    array_unique($request['spx_code'])));
                if (count($duplicateResSpx) > 0) {
                    $errors['errors']['dup_codes_spx'] = 'Encountered Duplicate in SPX';
                }
                foreach ($request['spx_code'] as $spxCodeKey => $spxCodeValue) {
                    if (strlen($spxCodeValue) < 3 || strlen($spxCodeValue) > 8) {
                        $errors['errors']['spx_code'][$spxCodeKey] = 'SPX CODE field only accept min of 3 and max of 8 characters';
                    }
                }
            }

            if (!empty($request['pdx_code'][0]) && !empty($request['pdx_code'][0])) {
                $duplicateResCombined = array_intersect($request['pdx_code'], $request['sdx_code']);
                if (count($duplicateResCombined) > 0) {
                    $errors['errors']['SDPDX_dup_codes'] = 'Encountered Duplicate PDX and SDX';
                }
            }

            if (!empty($request['ppx_code'][0]) && !empty($request['spx_code'][0])) {
                $duplicateResCombined = array_intersect($request['ppx_code'], $request['spx_code']);
                if (count($duplicateResCombined) > 0) {
                    $errors['errors']['PPXSPX_dup_codes'] = 'Encountered Duplicate PPX and SPX';
                }
            }
        }

        return $errors;
    }

    /**
     *Scenario 1
     * No file uploaded but updated account
     * rename old file to new account name
     *
     * Scenario 2
     * With file upload and updated account
     * Delete old file and upload new file with new account name
     *
     * Scenario 3
     * With file upload but did not update account
     * Overwrite old file with new one
     */

    public function processFile ($request, $action)
    {
        $caseId = $request->get('case_id');
        $case   = mCase::find($caseId);

        switch ($action) {
            case '1':
                Storage::disk('public')->put($request->get('account') . '.' . $request->file('file')->getClientOriginalExtension(), fopen($request->file('file'), 'r+'));
                break;
            case '2':
                if (!$request->file('file') && $case->account != $request->get('account')) {
                    if (Storage::disk('public')->exists($case->file)) {
                        Storage::disk('public')->move($case->file, $request->get('account') . '.' . $request->file('file')->getClientOriginalExtension());
                    }
                } else {
                    if ($request->file('file') && $case->account != $request->get('account')) {
                        if (Storage::disk('public')->exists($case->file)) {
                            Storage::disk('public')->delete($case->file);
                        }
                        Storage::disk('public')->put($request->get('account') . '.' . $request->file('file')->getClientOriginalExtension(),
                            fopen($request->file('file'), 'r+'));
                    } else {
                        if ($request->file('file') && $case->account == $request->get('account')) {
                            Storage::disk('public')->put($request->get('account') . '.' . $request->file('file')->getClientOriginalExtension(),
                                fopen($request->file('file'), 'r+'));
                        }
                    }
                }
                break;
            default:
                Log::error("processFile: Default switch");
                break;
        }
    }

    public function getCaseDetails ($request)
    {
        $whereValues = [
            'case_type_id' => $request['case_type_id'],
            'service_line_id' => $request['service_line_id'],
            'system' => $request['system'],
            'description' => '' . $request['description'] . ''
        ];

        return mCase::where($whereValues)->first();
    }
}

