<?php
/**
 * Created by PhpStorm.
 * User: ECVitug
 * Date: 6/3/2019
 * Time: 7:51 PM
 */

namespace App\Http\Repository;

use App\ClassPax;
use Illuminate\Support\Facades\DB;

class ClassPaxRepository
{

    public function getTableData ($classId)
    {
        $getJoin = [
            "trainees" => "LEFT JOIN assignments AS assign ON assign.id = answers.assignment_id LEFT JOIN  cases AS c ON assign.case_id = c.id",
            "nsCount" => "LEFT JOIN  cases AS c ON assignments.case_id = c.id"
        ];
        return classPax::join('users', 'class_paxes.user_id', 'users.id')
            ->leftJoin('answers', 'class_paxes.id', 'answers.class_pax_id')
            ->where('class_paxes.class_id', $classId)
            ->whereNull('class_paxes.deleted_at')
            ->select([
                'class_paxes.id',
                DB::raw('CONCAT(users.last_name, ", ", users.first_name, ", ", users.middle_name) AS user'),
                DB::raw("(SELECT count(answers.id) FROM answers ". $getJoin['trainees'] ." WHERE c.is_practice_case = 0 AND answers.status = 'completed' AND answers.class_pax_id = class_paxes.id) AS completed"),
                DB::raw("(SELECT count(answers.id) FROM answers ". $getJoin['trainees'] ." WHERE c.is_practice_case = 0 AND answers.status = 'in progress' AND answers.class_pax_id = class_paxes.id) AS in_progress"),
                DB::raw("(SELECT ABS((count(assignments.id) - completed - in_progress)) FROM assignments ". $getJoin['nsCount'] ." WHERE c.is_practice_case = 0 AND assignments.class_id = class_paxes.class_id) AS not_started"),
                DB::raw("CAST(COALESCE((SELECT ABS(SUM(answers.score)/count(answers.id)) FROM answers ". $getJoin['trainees'] ." WHERE c.is_practice_case = 0 AND answers.status = 'completed' AND answers.class_pax_id = class_paxes.id), 0) AS DECIMAL(18,2)) AS avg_score"),
                DB::raw("CAST(COALESCE((SELECT ABS(SUM(answers.poa_score)/count(answers.id)) FROM answers  ". $getJoin['trainees'] ."  WHERE c.is_practice_case = 0 AND answers.status = 'completed' AND answers.class_pax_id = class_paxes.id), 0) AS DECIMAL(18,2)) AS poa_score"),
            ])
            ->groupBy('class_paxes.id');
    }
}