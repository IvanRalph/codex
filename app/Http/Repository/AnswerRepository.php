<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 7/22/2019
 * Time: 5:50 PM
 */

namespace App\Http\Repository;

use App\mCase;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AnswerRepository
{
    protected $poaScore;

    public function __construct ()
    {
        $this->poaScore = 0;
    }

    public function calculateScore ($caseId, $answer)
    {
        $score = 0;

        $case = mCase::find($caseId)->keyset;
        
        $pdxAns = $answer['pdx'][0];

        $sdxAns = $answer['sdx'];

        $ppxAns = $answer['ppx'][0]['code'];

        $spxAns = $answer['spx'];

        $pdxScore = $this->calculatePdx($pdxAns, $case);

        $sdxScore = $this->calculateSdx($sdxAns, $case);

        $ppxScore = $this->calculatePpx($ppxAns, $case);

        $spxScore = $this->calculateSpx($spxAns, $case);

        $expectedCodes = count($case['pdx']) + count($case['sdx']) + count($case['ppx']) + count($case['spx']);

        $correctCodes = $pdxScore + $sdxScore + $ppxScore + $spxScore;

        return [
            'coding_score' => abs($correctCodes / $expectedCodes),
            'poa_score' => abs($this->poaScore / $correctCodes)
        ];
    }

    public function calculatePdx ($pdxAns, &$case)
    {
        $code = Arr::pluck($case['pdx'], 'code');

        $answerIndex = array_search($pdxAns['code'], $code);

        if (in_array($pdxAns['code'], $code)) {
            if ($case['pdx'][$answerIndex]['poa'] == $pdxAns['poa']) {
                $this->poaScore++;
            }

            Arr::forget($case, 'pdx.' . $answerIndex);

            foreach ($case['pdx'] as $pdx) {
                array_push($case['sdx'], $pdx);
            }

            return 1;
        } else {
            return 0;
        }
    }

    public function calculateSdx ($sdxAns, $case)
    {
        $score = 0;

        foreach ($sdxAns as $key => $val) {
            if (in_array($val['code'], Arr::pluck($case['sdx'], 'code'))) {
                if (($val['poa'] == $sdxAns[$key]['poa'])) {
                    $this->poaScore++;
                }
                $score++;
            } else {
                $score--;
            }
        }

        return $score;
    }

    public function calculatePpx ($ppxAns, $case)
    {
        return in_array($ppxAns, $case['ppx']) ? 1 : 0;
    }

    public function calculateSpx ($spxAns, $case)
    {
        $score = 0;

        foreach ($spxAns as $key => $val) {
            if (in_array($val['code'], $case['spx'])) {
                $score++;
            } else {
                $score--;
            }
        }

        return $score < 0 ? 0 : $score;
    }
}