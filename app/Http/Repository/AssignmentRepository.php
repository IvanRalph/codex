<?php
/**
 * Created by PhpStorm.
 * User: ECVitug
 * Date: 6/3/2019
 * Time: 7:51 PM
 */

namespace App\Http\Repository;

use App\Assignment;
use Illuminate\Support\Facades\DB;

class AssignmentRepository
{
    public function getTableData ($classId)
    {
        return Assignment::hydrate(DB::select('CALL get_tbl_class_cases('. $classId .')'));
//        return Assignment::join('cases', 'assignments.case_id', 'cases.id')
//            ->join('case_types', 'cases.case_type_id', 'case_types.id')
//            ->join('service_lines', 'cases.service_line_id', 'service_lines.id')
//            ->join('systems', 'cases.system', 'systems.id')
//            ->leftJoin('answers', 'assignments.id', 'answers.assignment_id')
//            ->where('assignments.class_id', $classId)
//            ->whereNull('cases.deleted_at')
//            ->select([
//                'cases.id',
//                'assignments.id AS assignment_id',
//                'case_types.description AS case_type_id',
//                'service_lines.description AS service_line_id',
//                'systems.description AS system',
//                'cases.description AS description',
//                DB::raw("COALESCE(CAST((SELECT count(class_pax_id) FROM answers ans WHERE ans.status = 'completed' AND ans.assignment_id = assignments.id)/(SELECT count(class_pax_id) FROM answers ans WHERE ans.assignment_id = assignments.id) * 100 AS DECIMAL(18, 0)), 0) AS completed_pct"),
//                DB::raw('COALESCE(CAST(SUM(answers.score)/count(answers.id) AS DECIMAL(18,2)), 0) AS avg_score')])
//            ->groupBy([
//                'cases.id',
//                'assignments.id',
//                'case_types.description',
//                'service_lines.description',
//                'systems.description',
//                'cases.description',
//                'answers.score',
//                'answers.id'
//            ]);
    }

    public function validateAssignment ($caseId, $classId)
    {
        return Assignment::whereCaseId($caseId)->whereClassId($classId)->first();
    }
}