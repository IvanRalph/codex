<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 5/9/2019
 * Time: 5:06 PM
 */

namespace App\Http\Repository;

use App\ClassPax;
use App\mClass;
use Illuminate\Support\Facades\DB;

class ClassRepository
{
    public function getTableData ()
    {
        return mClass::join('users', 'classes.created_by', 'users.id')
            ->whereNull('classes.deleted_at')
            ->select('classes.id', 'classes.description', 'classes.no_of_pax', 'classes.target_end',
                DB::raw('CONCAT(users.first_name, " ", users.last_name) AS created_by'), 'classes.created_at');
    }

    public function updateNoOfClassPax ($classId)
    {
        $classPaxCount = ClassPax::where('class_id', $classId)->distinct()->count('user_id');

        $res = mClass::find($classId)->update([
            'no_of_pax' => $classPaxCount
        ]);

        return $res;
    }
}