<?php
/**
 * Created by PhpStorm.
 * User: igvitto
 * Date: 7/16/2019
 * Time: 11:13 PM
 */

namespace App\Http\Repository;

use App\CaseLog;
use Illuminate\Support\Facades\DB;

class CaseLogRepository
{
    public function getLastLog ($caseId, $classId)
    {
        return CaseLog::whereCaseId($caseId)
            ->whereClassId($classId)
            ->whereUserId(auth()->user()->id)
            ->orderBy('id', 'DESC')
            ->select(DB::raw('TIME_TO_SEC(handling_time) AS handling_time'), 'created_at', 'status')
            ->first();
    }

    public function getHandlingTime ($caseId, $classId)
    {
        return CaseLog::whereCaseId($caseId)
            ->whereClassId($classId)
            ->whereUserId(auth()->user()->id)
            ->orderBy('id', 'DESC')
            ->where('status', '!=', 1)
            ->select(DB::raw('TIME_TO_SEC(handling_time) AS handling_time'), 'created_at', 'status')
            ->get();
    }
}