<?php

namespace App\Http\Controllers;

use App\CaseLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CaseLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CaseLog  $caseLog
     * @return \Illuminate\Http\Response
     */
    public function show(CaseLog $caseLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CaseLog  $caseLog
     * @return \Illuminate\Http\Response
     */
    public function edit(CaseLog $caseLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CaseLog  $caseLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CaseLog $caseLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CaseLog  $caseLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(CaseLog $caseLog)
    {
        //
    }
}
