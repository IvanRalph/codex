<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    public function __construct ()
    {
        $this->middleware('guest');
    }

    public function index ()
    {
        return view('layouts.login-master');
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('azure')->user();

        $cUser = User::where('username', $user->user['mailNickname'])->first();

        if ($cUser) {
            Auth::login($cUser, true);
        } else {
            abort(500);
        }

        return redirect('/');
    }
}
