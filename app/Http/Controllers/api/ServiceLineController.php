<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\ServiceLineRequest;
use App\ServiceLine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(ServiceLine::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceLineRequest $request)
    {
        $data = collect($request->only([
            'description',
            'case_type_id'
        ]));

        $data->put('created_by', auth()->user()->id);

        ServiceLine::create($data->toArray());

        return response()->json([
            'message' => 'Service Line created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(ServiceLine::whereCaseTypeId($id)->get());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(ServiceLine::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceLineRequest $request, $id)
    {
        $data = collect($request->only([
            'description',
            'case_type_id'
        ]));

        $data->put('updated_by', auth()->user()->id);

        ServiceLine::find($id)->update($data->toArray());

        return response()->json([
            'message' => 'Service Line updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceLine::find($id)->delete();

        return response()->json([
            'message' => 'Service Line deleted successfully'
        ]);
    }
}
