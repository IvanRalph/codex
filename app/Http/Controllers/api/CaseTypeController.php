<?php

namespace App\Http\Controllers\api;

use App\CaseType;
use App\Http\Requests\CaseTypeRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CaseTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index ()
    {
        return response()->json(CaseType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (CaseTypeRequest $request)
    {
        $data = collect($request->only([
            'description',
            'with_poa'
        ]));

        $data->put('created_by', auth()->user()->id);

        CaseType::create($data->toArray());

        return response()->json([
            'message' => 'Case Type created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id)
    {
        return response()->json(CaseType::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update (CaseTypeRequest $request, $id)
    {
        $data = collect($request->only([
            'description',
            'with_poa'
        ]));

        $data->put('updated_by', auth()->user()->id);

        CaseType::find($id)->update($data->toArray());

        return response()->json([
            'message' => 'Case Type updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id)
    {
        CaseType::find($id)->delete();

        return response()->json([
            'message'=>'Case Type deleted successfully'
        ]);
    }
}
