<?php

namespace App\Http\Controllers\Api;

use App\Assignment;
use App\Http\Requests\AssignmentRequest;
use App\Http\Requests\AssignmentTableRequest;
use App\Http\Requests\DeleteAssignmentRequest;
use http\Env\Response;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repository\AssignmentRepository;
use App\Http\Repository\CaseRepository;
use Illuminate\Support\Facades\Log;

class AssignmentController extends Controller
{
    protected $assignmentRepository, $caseRepository;

    public function __construct (AssignmentRepository $assignmentRepository, CaseRepository $caseRepository)
    {
        $this->assignmentRepository = $assignmentRepository;
        $this->caseRepository       = $caseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param AssignmentTableRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index (AssignmentTableRequest $request)
    {
        $data = $this->assignmentRepository->getTableData($request->get('class_id'));

        return datatables($data)
            ->addColumn('checkbox', function ($model) {
                return '<div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input dataCheckBox" id="customCheck' . $model->id . '" data-id="' . $model->assignment_id . '">
                  <label class="custom-control-label" for="customCheck' . $model->id . '"></label>
                </div>
                ';
            })
            ->editColumn('completed_pct', function ($model) {
                return '<div class="progress">
						  <div class="progress-bar progress-bar-striped progress-bar-animated " role="progressbar" aria-valuenow="' . $model->completed_pct . '" aria-valuemin="0" aria-valuemax="100" style="width: ' . $model->completed_pct . '%">' . $model->completed_pct . '%</div>
						</div>';
            })
            ->editColumn('avg_score', function ($model) {
                return '<span class="kt-badge kt-badge--unified-brand kt-badge--inline">' . $model->avg_score . '</span>';
            })
            ->rawColumns([
                'checkbox',
                'completed_pct',
                'avg_score'
            ])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AssignmentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store (AssignmentRequest $request)
    {
        try {
            $getCaseDetails = $this->caseRepository->getCaseDetails($request->all());

            if ($this->assignmentRepository->validateAssignment($getCaseDetails->id, $request->get('class_id'))) {
                return response()->json([
                    'message' => 'Data given was invalid',
                    'errors' => [
                        'case_type_id' => [
                            'Case already exists'
                        ],
                        'service_line_id' => [
                            'Case already exists'
                        ],
                        'system' => [
                            'Case already exists'
                        ],
                        'description' => [
                            'Case already exists'
                        ]
                    ]
                ], 422);
            }

            if ($getCaseDetails) {
                $data = collect($request->only([
                    'class_id',
                ]));

                $data->put('case_id', $getCaseDetails->id);
                $data->put('date_assigned', Carbon::now()->format('Y-m-d H:i:s'));
                $data->put('assigned_by', 1); // Default 1 for now, but it should be from auth() user id

                $assignmentDetails = Assignment::create($data->toArray());

                return $assignmentDetails ? response()->json([
                    'data'    => $assignmentDetails,
                    'message' => 'Case assigned successfully'
                ], $status = 201) : response()->json('No assignment was made.', 500);
            } else {
                return response()->json([
                    'data' => 'Case not found',
                ], 422);
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id)
    {
        Assignment::find($id)->delete();

        return response()->json([
            'data' => 'Case deleted successfully'
        ]);
    }

    public function bulkDelete (DeleteAssignmentRequest $request)
    {
        Assignment::whereIn('id', $request->get('ids'))->delete();

        return response()->json([
            'data'    => [],
            'message' => 'Case(s) deleted successfully'
        ]);
    }
}
