<?php

namespace App\Http\Controllers\api;

use App\ClassPax;
use App\Exceptions\GeneralException;
use App\Http\Repository\ClassRepository;
use App\Http\Repository\AssignmentRepository;
use App\Http\Repository\ClassPaxRepository;
use App\Exports\ClassExport;
use App\Http\Requests\ClassExportPDFRequest;
use App\Http\Requests\ClassEditRequest;
use App\Http\Requests\ClassExportCSVRequest;
use App\Http\Requests\ClassRequest;
use App\Http\Requests\ClassTableRequest;
use App\Http\Requests\DeleteClassRequest;
use App\mClass;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;

class ClassController extends Controller
{
    protected $classRepository, $assignmentRepository, $classPaxRepository;

    CONST classStatuses = ['open', 'closed', 'completed'];

    public function __construct (ClassRepository $classRepository, AssignmentRepository $assignmentRepository, ClassPaxRepository $classPaxRepository)
    {
        $this->classRepository      = $classRepository;
        $this->assignmentRepository = $assignmentRepository;
        $this->classPaxRepository   = $classPaxRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ClassTableRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index (ClassTableRequest $request)
    {
        $data = $this->classRepository->getTableData();

        return datatables($data)
            ->addColumn('checkbox', function ($model) {
                return '<div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input dataCheckBox" id="customCheck' . $model->id . '" data-id="' . $model->id . '">
                  <label class="custom-control-label" for="customCheck' . $model->id . '"></label>
                </div>
                ';
            })
            ->filterColumn('created_by', function ($query, $keyword) {
                $sql = "CONCAT(users.first_name, \" \", users.last_name)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->rawColumns(['checkbox'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (ClassRequest $request) // Converted to Request instead of ClassRequest for dev Purposes.
    {
        try {
            $data = collect($request->only([
                'description',
                'show_score',
                'show_answer',
                'show_rationale',
                'show_prev_ans',
                'passing_rate'
            ]));

            $data->put('created_by', auth()->user()->id);
            $data->put('no_of_pax', 0);
            $data->put('target_end', Carbon::parse($request->get('target_end')));
            $classDetails = mClass::create($data->toArray());

            return $classDetails ? response()->json([
                'data'    => $classDetails,
                'message' => 'Class created successfully'
            ], $status = 201) : response()->json('No Data Inserted', 500);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        return mClass::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id, ClassEditRequest $request)
    {
        $this->classRepository->updateNoOfClassPax($id);

        return mClass::join('users', 'classes.created_by', 'users.id')
            ->where('classes.id', $id)
            ->select([
                'classes.id AS id',
                'classes.description AS description',
                'classes.target_end AS target_end',
                DB::raw('CONCAT(users.last_name, ", ", users.first_name, ", ", users.middle_name) AS created_by'),
                'classes.no_of_pax AS no_of_pax',
                'classes.show_score',
                'classes.show_answer',
                'classes.show_rationale',
                'classes.show_prev_ans',
                'classes.created_at AS created_at',
                'classes.passing_rate AS passing_rate',
                DB::raw('(SELECT CONCAT(CAST(COALESCE((count(id)/(SELECT count(id) FROM assignments ass2 WHERE ass2.class_id = classes.id AND ass2.deleted_at IS NULL)) * 100, 0) AS DECIMAL(18,0)), "%") FROM answers ans WHERE ans.status = "completed" AND ans.assignment_id IN ((SELECT id FROM assignments ass WHERE ass.class_id = classes.id AND deleted_at IS NULL))) AS completed')
            ])
            ->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update (ClassRequest $request, $id)
    {
        try {
            $data = collect($request->only([
                'description',
                'no_of_pax',
                'show_score',
                'show_answer',
                'show_rationale',
                'show_prev_ans',
                'passing_rate'
            ]));

            $data->put('target_end', Carbon::parse($request->get('target_end')));

            $classDetails = mClass::find($id)->update($data->toArray());

            if ($classDetails) {
                return $classDetails ? response()->json([
                    'data'    => [
                        'id' => $id
                    ],
                    'message' => 'Class updated successfully'
                ], $status = 201) : response()->json('No Data Updated', 500);
            } else {
                throw new GeneralException();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DeleteClassRequest $request
     * @return \Illuminate\Http\Response
     */
    public function bulkDelete (DeleteClassRequest $request)
    {
        mClass::whereIn('id', $request->get('ids'))->delete();

        return response()->json([
            'message' => 'Class(es) deleted successfully'
        ]);
    }

    public function exportPdf ($class_id, ClassExportPDFRequest $request)
    {
        try {
            $classDetails = mClass::join('users', 'classes.created_by', 'users.id')
                ->where('classes.id', $class_id)
                ->select([
                    'classes.id AS id',
                    'classes.description AS description',
                    'classes.target_end AS target_end',
                    DB::raw('CONCAT(users.last_name, ", ", users.first_name, ", ", users.middle_name) AS created_by'),
                    'classes.no_of_pax AS no_of_pax',
                    'classes.show_score',
                    'classes.show_answer',
                    'classes.show_rationale',
                    'classes.show_prev_ans',
                    'classes.created_at AS created_at',
                    'classes.passing_rate AS passing_rate',
                    DB::raw('(SELECT CONCAT(CAST(COALESCE((count(id)/(SELECT count(id) FROM assignments ass2 WHERE ass2.class_id = classes.id AND ass2.deleted_at IS NULL)) * 100, 0) AS DECIMAL(18,0)), "%") FROM answers ans WHERE ans.status = "completed" AND ans.assignment_id IN ((SELECT id FROM assignments ass WHERE ass.class_id = classes.id AND deleted_at IS NULL))) AS completed')
                ])
                ->first();

            $cases    = $this->assignmentRepository->getTableData($class_id);
            $trainees = $this->classPaxRepository->getTableData($class_id)->get();

            if ($classDetails) {
                $pdf = PDF::loadView('layouts.class-pdf', compact('classDetails', 'cases', 'trainees'));

                return $pdf->download('Code_' . $classDetails->id . '.pdf');
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    public function exportCSV (ClassExportCSVRequest $request)
    {
        return Excel::download(new ClassExport($this->classRepository), 'class-management-' . Carbon::now()->format('m-d-Y') . '.csv');
    }

    public function getClassByAuthUser (Request $request)
    {
        $res = ClassPax::join('classes', 'class_paxes.class_id', 'classes.id')
            ->where('class_paxes.user_id', auth()->user()->id)
            ->select([
                'classes.id AS id',
                'classes.description AS description'
            ]);

        if ($request->get('with_closed') == "true") {
            $res->whereIn('classes.status', self::classStatuses);
        } else {
            $res->where('classes.status', 'open');
        }

        return response()->json($res->get());
    }
}
