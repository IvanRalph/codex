<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TextUploaderRequest;
use Illuminate\Support\Facades\Log;
use App\Reference;
use App\User;
use auth;
use Carbon\Carbon;

class ReferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filename      = 'icd10cm_codes.txt';
        $ctr           = 0;
        $arr_codes     = array();
        $arr_desc      = array();
        $arr_temp      = array();
        $arr_refResult = array();
        try {
            if (!file_exists(storage_path($filename))) {
                return response()->json('File not found.', 500);
            }

            $handle = fopen(storage_path($filename), "r");

            if ($handle) {
                while (!feof($handle)) {
                    $item        = fgets($handle, 4096);
                    $arr_codes[] = explode(' ', trim($item))[0];
                    $arr_temp[]  = explode(' ', trim($item));
                    unset($arr_temp[$ctr][0]);
                    $arr_desc[]      = trim(implode(" ", $arr_temp[$ctr]));
                    $arr_refResult[] = array(
                        "code"        => $arr_codes[$ctr],
                        "description" => $arr_desc[$ctr]
                    );
                    ++$ctr;
                }
                fclose($handle);
            } else {
                return response()->json('File open failed.', 500);
            }

            return response()->json($arr_refResult);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TextUploaderRequest $request)
    {
        //
        try{
            $getValidate = $request->validated();
            if ($request->hasFile('referenceFile')) {
                $file = $request->file('referenceFile');
                $fileName = "icd10cm_codes.txt";
                Storage::disk('reference')->delete($fileName);
                Storage::disk('reference')->put($fileName, file_get_contents($file));
                Reference::updateOrCreate([
                    'id' => 1
                ],[
                    'modified_by' => auth()->user()->id,
                    'updated_at'    => Carbon::now()->format('Y-m-d H:i:s')
                ]);
                return [
                    'message' => 'Uploaded reference successfully!'
                ];
            }
        
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $uploadLogs = Reference::orderBy('id','desc')->get();
        $getULogs = [];
        foreach($uploadLogs as $key => $ulogs){
            $userRow = User::where(['id' => $ulogs->modified_by])->first();
            $getULogs[] = [
               'fullname' => $userRow->first_name .' '. $userRow->last_name,
               'modified_date' => date('Y-m-d h:i:sa',strtotime($ulogs->updated_at))
            ];
        }
        return $getULogs;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
