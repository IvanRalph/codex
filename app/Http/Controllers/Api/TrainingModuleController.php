<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Assignment;
use App\CaseLog;
use App\ClassPax;
use App\Http\Controllers\Controller;
use App\Http\Repository\AnswerRepository;
use App\Http\Repository\CaseLogRepository;
use App\Http\Repository\CaseRepository;
use App\Http\Repository\ClassRepository;
use App\Http\Requests\CaseLogRequest;
use App\Http\Requests\WorkQueueTableRequest;
use App\mCase;
use App\mClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrainingModuleController extends Controller
{
    protected $caseRepository, $answerRepository, $classRepository;

    public function __construct (CaseRepository $caseRepository, AnswerRepository $answerRepository, ClassRepository $classRepository)
    {
        $this->caseRepository = $caseRepository;

        $this->answerRepository = $answerRepository;

        $this->classRepository = $classRepository;
    }

    public function index ($classId, WorkQueueTableRequest $request)
    {
        return $this->caseRepository->getTableData(true, $classId);
    }

    public function pause ($caseId, $classId)
    {
        $log = CaseLog::whereCaseId($caseId)
            ->whereUserId(auth()->user()->id)
            ->whereStatus(1)
            ->orderBy('id', 'DESC')->first();

        $prev = Carbon::parse($log->created_at);

        $now = Carbon::now();

        $handlingTime = gmdate("H:i:s", $prev->diffInSeconds($now));

        CaseLog::create([
            'case_id'       => $caseId,
            'user_id'       => auth()->user()->id,
            'class_id'      => $classId,
            'handling_time' => $handlingTime,
            'status'        => 2
        ]);
    }

    public function complete ($caseId, $classId, CaseLogRepository $caseLogRepository, CaseLogRequest $request)
    {
        $class = mClass::find($classId);

        $case = mCase::find($caseId);

        $score = $this->answerRepository->calculateScore($caseId, $request->get('keyset'));

        $totalHT = $caseLogRepository->getHandlingTime($caseId, $classId)->sum('handling_time');

        $lastLog = $caseLogRepository->getLastLog($caseId, $classId);

        $stopToSec = Carbon::parse($lastLog->created_at)->diffInSeconds(Carbon::now());

        $handlingTime = $totalHT + $stopToSec;

        $assignment = Assignment::whereClassId($classId)->whereCaseId($caseId)->first();

        $classPax = ClassPax::whereClassId($classId)->whereUserId(auth()->user()->id)->first();

        $data = collect([
            'assignment_id' => $assignment->id,
            'class_pax_id'  => $classPax->id,
            'status'        => 'completed',
            'last_saved'    => Carbon::now(),
            'score'         => ($score['coding_score'] * 100),
            'poa_score'     => $score['poa_score'],
            'last_computed' => Carbon::now()
        ]);

        $data->put('answers', $request->get('keyset'));

        DB::transaction(function () use ($data, $caseId, $classId, $handlingTime, $assignment, $classPax) {
            Answer::updateOrCreate(
                [
                    'assignment_id' => $assignment->id,
                    'class_pax_id'  => $classPax->id
                ],
                $data->toArray()
            );

            CaseLog::create([
                'case_id'       => $caseId,
                'user_id'       => auth()->user()->id,
                'class_id'      => $classId,
                'handling_time' => gmdate("H:i:s", $handlingTime),
                'status'        => 3
            ]);
        });

        $data = null;

        if ($class->show_score) {
            $data['score'] = $score;
        }

        if ($class->show_answer) {
            $data['answer'] = $request->get('keyset');
        }

        if ($class->show_rationale) {
            $data['rationale'] = $case->rationale;
        }

        return response()->json([
            'message' => 'Case submitted successfuly.',
            'data'    => $data
        ]);
    }

    public function save ($caseId, $classId, CaseLogRequest $request)
    {
        $assignment = Assignment::whereClassId($classId)->whereCaseId($caseId)->first();

        $classPax = ClassPax::whereClassId($classId)->whereUserId(auth()->user()->id)->first();

        $data = collect([
            'assignment_id' => $assignment->id,
            'class_pax_id'  => $classPax->id,
            'status'        => 'in progress',
            'last_saved'    => Carbon::now(),
            'last_computed' => Carbon::now()
        ]);

        $data->put('answers', $request->get('keyset'));

        DB::transaction(function () use ($assignment, $classPax, $data) {
            Answer::updateOrCreate(
                [
                    'assignment_id' => $assignment->id,
                    'class_pax_id'  => $classPax->id
                ],
                $data->toArray()
            );
        });

        return response()->json([
            'message' => 'Changes has been saved.'
        ]);
    }

    public function getPreviousAnswer ($caseId, $classId)
    {
        $classPaxId = ClassPax::whereUserId(auth()->user()->id)->pluck('id');

        $assignmentId = Assignment::whereCaseId($caseId)->pluck('id');

        return Answer::whereIn('class_pax_id', $classPaxId)->whereIn('assignment_id', $assignmentId)->where('status', '!=', 'in progress')->orderBy('created_at', 'DESC')->firstOrFail();
    }

    public function getSavedAnswer ($caseId, $classId)
    {
        $assignment = Assignment::whereClassId($classId)->whereCaseId($caseId)->first();

        $classPax = ClassPax::whereClassId($classId)->whereUserId(auth()->user()->id)->first();

        return Answer::whereAssignmentId($assignment ? $assignment->id : 0)->whereClassPaxId($classPax ? $classPax->id : 0)->orderBy('created_at', 'DESC')->first();
    }
}
