<?php

namespace App\Http\Controllers\api;

use App\Http\Requests\UserDeleteRequest;
use App\Http\Requests\UserEditRequest;
use App\Http\Requests\UserIndexRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserIndexRequest $request
     * @return User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index (UserIndexRequest $request)
    {
        return User::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store (UserRequest $request)
    {
        $user = User::create($request->all());

        $user->assignRole($request->get('role_id'));

        return response()->json([
            'message' => 'User created successfully.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int            $id
     * @param UserEditRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit ($id, UserEditRequest $request)
    {
        $user          = User::find($id);
        $user->role_id = $user->roles->pluck('id');

        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update (UserRequest $request, $id)
    {
        $user = User::find($id);
        $user->update($request->all());

        $role = Role::find($request->get('role_id'))->first();

        $user->assignRole($role->name);

        return response()->json([
            'message' => 'User updated successfully.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int              $id
     * @param UserDeleteRequest $request
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id, UserDeleteRequest $request)
    {
        User::find($id)->delete();

        return response()->json([
            'message' => 'User deleted successfully.'
        ]);
    }

    public function authUser ()
    {
        return response()->json(auth()->user()->full_name);
    }

    public function getPermissions ()
    {
        $roles = auth()->user()->roles;
        $myRoles = !$roles->isEmpty() ? $roles->pluck('id')->implode(',') : '0';

        $perms = DB::table('permissions')
            ->leftJoin('role_has_permissions', 'permissions.id', 'role_has_permissions.permission_id')
            ->leftJoin('roles', 'role_has_permissions.role_id', 'roles.id')
            ->select([
                'permissions.name',
                DB::raw('CASE WHEN role_has_permissions.role_id IN (' . $myRoles .') THEN 1 ELSE 0 END AS has_access')
            ])
            ->groupBy('permissions.name', 'role_has_permissions.permission_id')
            ->get();

        return response()->json($perms);
    }
}
