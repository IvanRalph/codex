<?php

namespace App\Http\Controllers\Api;

use App\Http\Repository\ClassRepository;
use App\Http\Requests\ClassPaxDeleteRequest;
use App\Http\Requests\ClassPaxRequest;
use App\Http\Requests\ClassPaxTableRequest;
use App\mClass;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Repository\ClassPaxRepository;
use App\ClassPax;
use Illuminate\Support\Facades\Log;

class ClassPaxController extends Controller
{
    protected $classPaxRepository, $classRepository;

    public function __construct (ClassPaxRepository $classPaxRepository, ClassRepository $classRepository)
    {
        $this->classPaxRepository = $classPaxRepository;

        $this->classRepository = $classRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index (ClassPaxTableRequest $request)
    {
        $data = $this->classPaxRepository->getTableData($request->get('class_id'));

        return datatables($data)
            ->addColumn('checkbox', function ($model) {
                return '<div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input dataCheckBox" id="customCheck' . $model->id . '" data-id="' . $model->id . '">
                  <label class="custom-control-label" for="customCheck' . $model->id . '"></label>
                </div>
                ';
            })
            ->filterColumn('user', function ($query, $keyword) {
                $sql = "CONCAT(users.last_name, \", \", users.first_name, \", \", users.middle_name)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('completed', function ($query, $keyword) {
                $sql = "(SELECT count(id) FROM answers WHERE status = 'completed' AND class_pax_id = class_paxes.id)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('in_progress', function ($query, $keyword) {
                $sql = "(SELECT count(id) FROM answers WHERE status = 'in progress' AND class_pax_id = class_paxes.id)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('not_started', function ($query, $keyword) {
                $sql = "(SELECT count(id) FROM answers WHERE status = 'not started' AND class_pax_id = class_paxes.id)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->filterColumn('avg_score', function ($query, $keyword) {
                $sql = "CAST(COALESCE((SELECT SUM(score)/count(id) FROM answers WHERE status = 'completed' AND class_pax_id = class_paxes.id), 0) AS DECIMAL(18,2))  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->rawColumns(['checkbox'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ClassPaxRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store (ClassPaxRequest $request)
    {
        try {
            $data = collect($request->only([
                'class_id',
                'user_id'
            ]));

            $classPaxDetails = ClassPax::create($data->toArray());

            $this->classRepository->updateNoOfClassPax($request->get('class_id'));

            return $classPaxDetails ? response()->json([
                'data'    => $classPaxDetails,
                'message' => 'Trainee assigned successfully'
            ], $status = 201) : response()->json('No Class Pax was made.', 500);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit ($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id)
    {
        ClassPax::find($id)->delete();

        return response()->json([
            'data' => 'Class Pax Deleted Successfully'
        ]);
    }

    public function bulkDelete (ClassPaxDeleteRequest $request)
    {
        ClassPax::whereIn('id', $request->get('ids'))->delete();

        return response()->json([
            'message' => 'Trainee removed successfully'
        ]);
    }
}
