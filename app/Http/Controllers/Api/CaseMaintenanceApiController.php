<?php

namespace App\Http\Controllers\Api;

use App\Answer;
use App\Assignment;
use App\CaseLog;
use App\ClassPax;
use App\Exceptions\GeneralException;
use App\Exports\CaseExport;
use App\Http\Repository\CaseLogRepository;
use App\Http\Repository\CaseRepository;
use App\Http\Requests\CaseEditRequest;
use App\Http\Requests\CaseExportCSVRequest;
use App\Http\Requests\CaseExportPDFRequest;
use App\Http\Requests\CaseTableRequest;
use App\Http\Requests\DeleteCaseRequest;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\mCase;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\CaseRequest;
use Barryvdh\DomPDF\Facade as PDF;

class CaseMaintenanceApiController extends Controller
{
    protected $caseRepository;

    public function __construct (CaseRepository $caseRepository)
    {
        $this->caseRepository = $caseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param CaseTableRequest $request
     * @return mCase[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index (CaseTableRequest $request)
    {
        $cases = $this->caseRepository->getTableData();

        return datatables($cases)
            ->addColumn('checkbox', function ($model) {
                return '<div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input dataCheckBox" id="customCheck' . $model->id . '" data-id="' . $model->id . '">
                  <label class="custom-control-label" for="customCheck' . $model->id . '"></label>
                </div>
                ';
            })
            ->filterColumn('updated_by', function ($query, $keyword) {
                $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                $query->whereRaw($sql, ["%{$keyword}%"]);
            })
            ->rawColumns(['checkbox'])
            ->toJson();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create ()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CaseRequest $request
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     */
    public function store (CaseRequest $request)
    {
        try {
            $validatePoa = $this->caseRepository->validatePoa($request->all());

            if (array_key_exists('errors', $validatePoa)) {
                return response()->json($validatePoa, 422);
            }

            $data = collect($request->only([
                'description',
                'case_type_id',
                'service_line_id',
                'system',
                'facility',
                'account',
                'patient',
                'rationale',
            ]));

            $pdx_code = $request->get('pdx_code');
            $pdx_poa  = $request->get('pdx_poa');
            $sdx_code = $request->get('sdx_code');
            $sdx_poa  = $request->get('sdx_poa');
            $ppx_code = $request->get('ppx_code');
            $spx_code = $request->get('spx_code');

            $this->caseRepository->processFile($request, 1);

            $keyset = [];

            for ($i = 0; $i < count($pdx_code); $i++) {
                $keyset['pdx'][$i] = [
                    'poa'  => $pdx_poa[$i],
                    'code' => $pdx_code[$i]
                ];
            }

            for ($i = 0; $i < count($sdx_code); $i++) {
                $keyset['sdx'][$i] = [
                    'poa'  => $sdx_poa[$i],
                    'code' => $sdx_code[$i]
                ];
            }

            for ($i = 0; $i < count($ppx_code); $i++) {
                $keyset['ppx'][$i] = $ppx_code[$i];
            }

            for ($i = 0; $i < count($spx_code); $i++) {
                $keyset['spx'][$i] = $spx_code[$i];
            }

            $data->put('keyset', $keyset);
            $data->put('file', $request->get('account') . '.pdf');
            $data->put('discharge', $request->get('discharge') ? Carbon::parse($request->get('discharge')) : null);
            $data->put('admission', $request->get('admission') ? Carbon::parse($request->get('admission')) : null);
            $data->put('updated_by', auth()->user()->id);
            $data->put('is_practice_case', $request->get('is_practice_case') ? 1 : 0);

            $caseDetails = mCase::create($data->toArray());

            return $caseDetails ? response()->json([
                'data'    => $caseDetails,
                'message' => 'Case created successfully'
            ], $status = 201) : response()->json('No Data inserted', 500);
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show ($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int            $id
     * @param CaseEditRequest $request
     * @return \Illuminate\Http\Response
     */
    public function edit ($id, CaseEditRequest $request)
    {
        return mCase::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CaseRequest $request
     * @param  int        $id
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     */
    public function update (CaseRequest $request, $id)
    {
        $validatePoa = $this->caseRepository->validatePoa($request->all());

        if (array_key_exists('errors', $validatePoa)) {
            return response()->json($validatePoa, 422);
        }

        try {
            $data = collect($request->only([
                'description',
                'file',
                'case_type_id',
                'service_line_id',
                'system',
                'facility',
                'account',
                'patient',
                'discharge',
                'admission',
                'rationale',
                'is_practice_case'
            ]));

            $pdx_code = $request->get('pdx_code');
            $pdx_poa  = $request->get('pdx_poa');
            $sdx_code = $request->get('sdx_code');
            $sdx_poa  = $request->get('sdx_poa');
            $ppx_code = $request->has('ppx_code') ? $request->get('ppx_code') : [""];
            $spx_code = $request->has('spx_code') ? $request->get('spx_code') : [""];

            $this->caseRepository->processFile($request, 2);

            $keyset = [];

            for ($i = 0; $i < count($pdx_code); $i++) {
                $keyset['pdx'][$i] = [
                    'poa'  => $pdx_poa[$i],
                    'code' => $pdx_code[$i]
                ];
            }

            for ($i = 0; $i < count($sdx_code); $i++) {
                $keyset['sdx'][$i] = [
                    'poa'  => $sdx_poa[$i],
                    'code' => $sdx_code[$i]
                ];
            }

            for ($i = 0; $i < count($ppx_code); $i++) {
                $keyset['ppx'][$i] = $ppx_code[$i];
            }

            for ($i = 0; $i < count($spx_code); $i++) {
                $keyset['spx'][$i] = $spx_code[$i];
            }

            $data->put('keyset', $keyset);
            $data->put('file', $request->get('account') . '.pdf');
            $data->put('discharge', $request->get('discharge') ? Carbon::parse($request->get('discharge')) : null);
            $data->put('admission', $request->get('admission') ? Carbon::parse($request->get('admission')) : null);
            $data->put('updated_by', auth()->user()->id);
            $data->put('is_practice_case', $request->get('is_practice_case') ? 1 : 0);

            $caseDetails = mCase::find($id)->update($data->toArray());

            if ($caseDetails) {
                return $caseDetails ? response()->json([
                    'data'    => $caseDetails,
                    'message' => 'Case updated successfully'
                ], $status = 201) : response()->json('No Data Updated', 500);
            } else {
                throw new GeneralException();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            throw new GeneralException();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy ($id)
    {
        mCase::find($id)->delete();

        return response()->json([
            'data' => 'Case deleted successfully'
        ]);
    }

    public function bulkDelete (DeleteCaseRequest $request)
    {
        mCase::whereIn('id', $request->get('ids'))->delete();

        return response()->json([
            'data'    => [],
            'message' => 'Case(s) deleted successfully'
        ]);
    }

    public function downloadPDF ($caseId)
    {
        $filenameToDownload = mCase::find($caseId)->file;

        $file = storage_path() . "\app\public\\" . $filenameToDownload;

        $headers = array(
            'Content-Type: application/pdf',
        );

        return response()->download($file, $filenameToDownload, $headers);
    }

    public function exportPDF ($caseId, CaseExportPDFRequest $request)
    {
        try {
            $caseDetails  = mCase::find($caseId);
            $otherDetails = array(
                'caseType'    => mCase::find($caseId)->caseType->description,
                'serviceLine' => mCase::find($caseId)->serviceLine->description,
                'systems'     => mCase::find($caseId)->systems->description,
            );

            $keyset = $caseDetails->keyset;

            if ($caseDetails) {
                $pdf = PDF::loadView('layouts.case-maintenance-pdf', compact('caseDetails', 'keyset', 'otherDetails'));

                return $pdf->download('Case_' . $caseDetails->id . '.pdf');
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());

            return response()->json($e->getMessage(), 500);
        }
    }

    public function exportCSV (CaseExportCSVRequest $request)
    {
        return Excel::download(new CaseExport($this->caseRepository),
            'case-maintenance-' . Carbon::now()->format('m-d-Y') . '.csv');
    }

    public function getByField ($fieldName, $caseTypeId = null, $serviceLineId = null, $systemId = null)
    {
        switch ($fieldName) {
            case 'case_type_id' :
                return mCase::join('case_types', 'cases.case_type_id', 'case_types.id')
                    ->select('cases.case_type_id AS id', 'case_types.description AS description')
                    ->groupBy([
                        'cases.case_type_id',
                        'case_types.description',
                    ])
                    ->get();
                break;
            case 'service_line_id' :
                return mCase::join('service_lines', 'cases.service_line_id', 'service_lines.id')
                    ->where('cases.case_type_id', $caseTypeId)
                    ->select('cases.service_line_id AS id', 'service_lines.description AS description')
                    ->groupBy([
                        'cases.service_line_id',
                        'service_lines.description',
                    ])
                    ->get();
                break;
            case 'system':
                return mCase::join('systems', 'cases.system', 'systems.id')
                    ->where('case_type_id', $caseTypeId)
                    ->where('service_line_id', $serviceLineId)
                    ->select('cases.system AS id', 'systems.description AS description')
                    ->groupBy([
                        'cases.system',
                        'systems.description'
                    ])
                    ->get();
                break;
            case 'description':
                return mCase::select('description')
                    ->where('case_type_id', $caseTypeId)
                    ->where('service_line_id', $serviceLineId)
                    ->where('system', $systemId)
                    ->groupBy('description')->get();
                break;
        }
    }

    public function handlingTime ($caseId, $classId, CaseLogRepository $caseLogRepository)
    {
        $lastLog = $caseLogRepository->getLastLog($caseId, $classId);

        $lastHT = $caseLogRepository->getHandlingTime($caseId, $classId);

        if (!$lastLog || ($lastLog && $lastLog->status == 2)) {
            $lastLog = CaseLog::create([
                'case_id'       => $caseId,
                'user_id'       => auth()->user()->id,
                'class_id'      => $classId,
                'handling_time' => '00:00:00',
                'status'        => 1
                //start
            ]);

            if ($lastLog) {
                Answer::updateOrCreate([
                        'assignment_id' => Assignment::whereClassId($classId)->whereCaseId($caseId)->first()->id,
                        'class_pax_id' => ClassPax::whereClassId($classId)->whereUserId(auth()->user()->id)->first()->id
                    ],
                    [
                        'status' => 'in progress',
                        'last_saved' => Carbon::now()
                ]);
            }
        }

        $lastLog->case = mCase::find($caseId);

        if ($lastHT->count() > 1) {
            $lastLog->last_ht = $lastHT->sum('handling_time');
        } else {
            $lastLog->last_ht = count($lastHT) > 0 ? $lastHT->first()->handling_time : 0;
        }

        return response()->json($lastLog);
    }
}
