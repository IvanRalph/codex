<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CaseTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return auth()->user()->hasPermissionTo('Add Case Types');
                }
            case 'PUT':
            case 'PATCH':
                {
                    return auth()->user()->hasPermissionTo('Edit Case Types');
                }
            default:
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'with_poa' => 'required|in:0,1'
        ];
    }
}
