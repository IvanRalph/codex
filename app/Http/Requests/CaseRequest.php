<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CaseRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        $case_id = $this->request->get('case_id');
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'file'            => 'required|file|mimes:pdf|max:5120',
                        'case_type_id'    => 'required|numeric',
                        'service_line_id' => 'required|numeric',
                        'system'          => 'required',
                        'facility'        => 'max:50',
                        'patient'         => 'max:50',
                        'description'     => 'required|max:50',
                        'account'         => 'required|digits_between:0,10|numeric|unique:cases,account,NULL,id,deleted_at,NULL',
                        'discharge'       => 'nullable|date|after:admission',
                        'admission'       => 'nullable|date|before:discharge',
                        'rationale'       => 'required'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'file'            => 'nullable|sometimes|file|mimes:pdf|max:5120',
                        'case_type_id'    => 'required|numeric',
                        'service_line_id' => 'required|numeric',
                        'system'          => 'required',
                        'facility'        => 'max:50',
                        'patient'         => 'max:50',
                        'description'     => 'required|max:50',
                        'account'         => [
                            'required',
                            'digits_between:0,10',
                            'numeric',
                            'unique:cases,account,'.$case_id.',id,deleted_at,NULL'
                            //Rule::unique('cases', 'account')->ignore($this->request->get('case_id'))
                        ],
                        'discharge'       => 'nullable|date|after:admission',
                        'admission'       => 'nullable|date|before:discharge',
                        'rationale'       => 'required'
                    ];
                }
            default:
                break;
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return auth()->user()->hasPermissionTo('Add Case');
                }
            case 'PUT':
            case 'PATCH':
                {
                    return auth()->user()->hasPermissionTo('Edit Case');
                }
            default:
                break;
        }
    }

    public function attributes ()
    {
        return [
            'case_type_id'    => 'case type',
            'service_line_id' => 'service line'
        ];
    }

    public function messages ()
    {
        return [
            'file.max' => "Maximum file size upload is 5MB (5120 KB)."
        ];
    }
}
