<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceLineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return auth()->user()->hasPermissionTo('Add Service Lines');
                }
            case 'PUT':
            case 'PATCH':
                {
                    return auth()->user()->hasPermissionTo('Edit Service Lines');
                }
            default:
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'case_type_id' => 'required|exists:case_types,id'
        ];
    }
}
