<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('Add Users');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'role_id' => 'required|exists:roles,id'
        ];
    }

    public function attributes ()
    {
        return [
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'NT Login',
            'role_id' => 'Role'
        ];
    }
}
