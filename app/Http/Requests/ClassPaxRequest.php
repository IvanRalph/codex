<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClassPaxRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return auth()->user()->hasPermissionTo('Assign Trainee to Class');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'user_id' => [
                'required',
                Rule::unique('class_paxes')->where(function ($query) {
                    return $query->where('user_id', $this->request->get('user_id'))
                        ->where('class_id', $this->request->get('class_id'))
                        ->whereNull('deleted_at');
                })
            ]
        ];
    }

    public function messages ()
    {
        return [
            'user_id.required' => 'Please select a trainee',
            'user_id.unique'   => 'Trainee already exists in this Class'
        ];
    }
}
