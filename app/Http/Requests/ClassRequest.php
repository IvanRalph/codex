<?php
/**
 * Created by PhpStorm.
 * User: ecvitug
 * Date: 5/22/2019
 * Time: 8:06 PM
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        switch ($this->method()) {
            case 'GET':
            case 'DELETE':
                {
                    return [];
                }
            case 'POST':
                {
                    return [
                        'description'    => 'required|max:50',
                        'target_end'     => 'required|date|after:tomorrow',
                        'show_score'     => 'required|boolean',
                        'show_answer'    => 'required|boolean',
                        'show_rationale' => 'required|boolean',
                        'show_prev_ans'  => 'required|boolean'
                    ];
                }
            case 'PUT':
            case 'PATCH':
                {
                    return [
                        'description'    => 'required|max:50',
                        'target_end'     => 'required|date',
                        'show_score'     => 'required|boolean',
                        'show_answer'    => 'required|boolean',
                        'show_rationale' => 'required|boolean',
                        'show_prev_ans'  => 'required|boolean'
                    ];
                }
            default:
                break;
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        switch ($this->method()) {
            case 'POST':
                {
                    return auth()->user()->hasPermissionTo('Add Class');
                }
            case 'PUT':
            case 'PATCH':
                {
                    return auth()->user()->hasPermissionTo('Edit Class');
                }
            default:
                return false;
        }
    }

    public function messages ()
    {
        return [
            'target_end.after' => 'The target completion must be a date after today'
        ];
    }
}
