<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'case_type_id'    => 'required|exists:cases,case_type_id',
            'service_line_id' => 'required|exists:cases,service_line_id',
            'system'          => 'required|exists:cases,system',
            'description'     => 'required|exists:cases,description',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        return auth()->user()->hasPermissionTo('Assign Cases to Class');
    }
}
