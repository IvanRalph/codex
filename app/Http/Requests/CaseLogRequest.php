<?php

namespace App\Http\Requests;

use App\CaseLog;
use App\Rules\NotEmpty;
use App\Rules\POARequiredIfSDX;
use App\Rules\SDXNotEqualPDX;
use App\Rules\SDXNotRepeated;
use App\Rules\SPXNotRepeated;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;

class CaseLogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize ()
    {
        $caselog = CaseLog::whereCaseId($this->request->get('case_id'))->whereUserId(auth()->user()->id)->whereClassId($this->request->get('class_id'))->whereStatus(3)->first();

        if ($caselog) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules ()
    {
        return [
            'keyset.pdx.*.code'        => [
                new NotEmpty(),
                'min:3',
                'max:8'
            ],
            'keyset.pdx.*.poa'         => [
                'in:Y,N,E,U,W,""',
            ],
            'keyset.pdx.*.description' => [
                'nullable',
                'max:50'
            ],
            'keyset.sdx.*.code'        => [
                'nullable',
                'min:3',
                'max:8',
                new SDXNotRepeated(Arr::pluck($this->request->get('keyset')['sdx'], 'code')),
                new SDXNotEqualPDX(Arr::pluck($this->request->get('keyset')['pdx'], 'code'))
            ],
            'keyset.sdx.*.poa'         => [
//                new POARequiredIfSDX(Arr::pluck($this->request->get('keyset')['sdx'], 'code')),
                'in:Y,N,E,U,W,""',
            ],
            'keyset.sdx.*.description' => [
                'nullable',
                'max:50'
            ],
            'keyset.ppx.*.code'        => [
                'nullable',
                'min:3',
                'max:8'
            ],
            'keyset.ppx.*.description' => [
                'nullable',
                'max:50'
            ],
            'keyset.spx.*.code'        => [
                'nullable',
                'min:3',
                'max:8',
                new SPXNotRepeated(Arr::pluck($this->request->get('keyset')['spx'], 'code')),
                Rule::notIn(Arr::pluck($this->request->get('keyset')['ppx'], 'code')),
            ],
            'keyset.spx.*.description' => [
                'nullable',
                'max:50'
            ],
        ];
    }

    public function messages ()
    {
        return [
            'keyset.spx.*.code.not_in' => 'SPX Code must not have any duplicate in PPX.'
        ];
    }

    public function attributes ()
    {
        return [
            'keyset.pdx.*.code'        => 'PDX Code',
            'keyset.pdx.*.poa'         => 'PDX POA',
            'keyset.pdx.*.description' => 'PDX Description',
            'keyset.sdx.*.code'        => 'SDX Code',
            'keyset.sdx.*.poa'         => 'SDX POA',
            'keyset.sdx.*.description' => 'SDX Description',
            'keyset.ppx.*.code'        => 'PPX Code',
            'keyset.ppx.*.description' => 'PPX Description',
            'keyset.spx.*.code'        => 'SPX Code',
            'keyset.spx.*.description' => 'SPX Description'
        ];
    }
}
