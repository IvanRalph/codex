<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class TextUploaderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasPermissionTo('Replace Reference');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->hasFile('referenceFile')) {
            $fsize = request()->file('referenceFile')->getSize();
            if($fsize == 0){
                return [
                    'referenceFile' => ['required']
                ];
            }
        }
        return [
            'referenceFile' => ['required','mimetypes:text/plain']
        ];
    }

    public function messages ()
    {
        return [
            'referenceFile.required' => "The reference file is required.",
            'referenceFile.mimetypes' => "The extension file that you are trying to upload is not allowed."
        ];
    }

}
