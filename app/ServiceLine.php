<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceLine extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'description',
        'case_type_id',
        'created_by',
        'updated_by'
    ];
}
