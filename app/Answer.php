<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $fillable
        = [
            'assignment_id',
            'class_pax_id',
            'status',
            'answers',
            'last_saved',
            'score',
            'poa_score',
            'last_computed',
        ];

    protected $casts = [
        'answers' => 'array'
    ];
}
