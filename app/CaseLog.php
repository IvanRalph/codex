<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaseLog extends Model
{
    public $timestamps
        = [
            'created_at',
            'updated_at',
        ];

    public $fillable
        = [
            'case_id',
            'user_id',
            'class_id',
            'status',
            'handling_time',
        ];
}
