<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ClassPax extends Model
{

    use SoftDeletes;

    protected $table = 'class_paxes';

    protected $fillable = ([
        'class_id',
        'user_id'
    ]);
}
