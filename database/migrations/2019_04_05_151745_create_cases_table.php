<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->string('file');
            $table->unsignedBigInteger('case_type_id');
            $table->unsignedBigInteger('service_line_id');
            $table->unsignedBigInteger('system');
            $table->string('facility')->nullable();
            $table->string('account')->nullable();
            $table->string('patient')->nullable();
            $table->dateTime('discharge')->nullable();
            $table->dateTime('admission')->nullable();
            $table->longText('keyset');
            $table->longText('rationale');
            $table->smallInteger('is_practice_case')->default(0);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('updated_by')->nullable();

            $table->foreign('updated_by')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('service_line_id')
                ->references('id')->on('service_lines')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('case_type_id')
                ->references('id')->on('case_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('system')
                ->references('id')->on('systems')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
