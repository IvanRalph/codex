<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllPassedCountView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        DB::unprepared('DROP VIEW IF EXISTS all_passed_count; CREATE VIEW `all_passed_count` AS
    SELECT 
        `class_paxes`.`id` AS `class_paxes_id`,
        `classes`.`id` AS `id`,
        `class_paxes`.`user_id` AS `user_id`,
        `classes`.`passing_rate` AS `passing_rate`,
        `cases`.`id` AS `case_id`,
        COALESCE(`answers`.`score`, 0) AS `score`,
        IF((`answers`.`score` >= `classes`.`passing_rate`),
            \'passed\',
            \'failed\') AS `passed`
    FROM
    assignments
    LEFT JOIN cases ON assignments.case_id = cases.id
    LEFT JOIN classes ON assignments.class_id = classes.id
    LEFT JOIN answers ON assignments.id = answers.assignment_id
    LEFT JOIN class_paxes ON answers.class_pax_id = class_paxes.id
    LEFT JOIN users ON class_paxes.user_id = users.id');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
    }
}
