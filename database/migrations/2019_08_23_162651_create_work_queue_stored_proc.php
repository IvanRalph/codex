<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkQueueStoredProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS get_tbl_training; CREATE PROCEDURE `get_tbl_training`(
            IN user_id bigint(20),
            IN class_id bigint(20)
            )
            BEGIN
            
            -- INT : User ID
            SELECT user_id into @user_id; 
            -- INT : Class ID
            SELECT class_id INTO @class_id;
            
            DROP temporary TABLE IF EXISTS tblmaxlog1;
            set @qry1 =concat(\'CREATE TEMPORARY TABLE tblmaxlog1 (SELECT MAX(id) as `maxid`,SEC_TO_TIME(SUM(TIME_TO_SEC(handling_time))) as handling_time,user_id,case_id,class_id FROM case_logs WHERE class_id = \',@class_id,\' and user_id = \',@user_id,\' GROUP BY case_id)\');
            
            PREPARE stmt from @qry1;
            Execute stmt;
            Deallocate prepare stmt;
            
            DROP temporary TABLE IF EXISTS tblmaxlog;
            CREATE TEMPORARY TABLE tblmaxlog (
            SELECT 
               tblmaxlog1.`maxid`,tblmaxlog1.handling_time,tblmaxlog1.user_id,tblmaxlog1.case_id,tblmaxlog1.class_id,case_logs.status
            FROM tblmaxlog1
                    left JOIN
                case_logs on case_logs.id = tblmaxlog1.maxid);
            
            set @qry = concat(\'SELECT 
                `cases`.`id` AS `id`,
                `case_types`.`description` AS `case_type_id`,
                `service_lines`.`description` AS `service_line_id`,
                `systems`.`description` AS `system`,
                `cases`.`facility` AS `facility`,
                `cases`.`description` AS `description`,
                COALESCE(answers.status, \'\'Not yet started\'\') AS status,
                COALESCE(answers.last_saved, NULL) AS last_saved,
                `tblmaxlog`.`handling_time`,
                `answers`.`last_computed` AS `date_submitted`,
                `answers`.`score` AS `score`,
                `answers`.`poa_score` AS `poa_score`
            FROM
                class_paxes
                    INNER JOIN
                users ON users.id = class_paxes.user_id
                    INNER JOIN
                assignments ON assignments.class_id = class_paxes.class_id
                    INNER JOIN
                cases ON cases.id = assignments.case_id
                    LEFT JOIN
                tblmaxlog ON tblmaxlog.user_id = users.id
                    AND tblmaxlog.case_id = cases.id
                    INNER JOIN
                `case_types` ON `cases`.`case_type_id` = `case_types`.`id`
                    INNER JOIN
                `service_lines` ON `cases`.`service_line_id` = `service_lines`.`id`
                    INNER JOIN
                `systems` ON `cases`.`system` = `systems`.`id`
                    left JOIN
                `answers` ON `answers`.`assignment_id` = `assignments`.`id`
                and `answers`.`class_pax_id` = `class_paxes`.`id`
            WHERE
                users.id = \',@user_id,\'
                and assignments.class_id = \',@class_id,\'
                        and (tblmaxlog.status != 3 or  tblmaxlog.status IS NULL)
                        and cases.deleted_at IS NULL
                        and assignments.deleted_at IS NULL
                
            GROUP BY users.id , cases.id;\');
            
            PREPARE stmt from @qry;
            Execute stmt;
            Deallocate prepare stmt;
            END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
    }
}
