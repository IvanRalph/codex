<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CasesStoredProc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS get_tbl_class_cases; CREATE PROCEDURE `get_tbl_class_cases`(
                        IN class_id int(20)
                        )
            BEGIN
            SELECT 
                c.id,
                ct.description AS \'case_type_id\',
                s.description AS \'service_line_id\',
                sys.description AS \'system\',
                c.description,
                COALESCE(CAST(count(answers) / (SELECT count(*) FROM class_paxes where class_paxes.class_id = class_id) * 100 AS DECIMAL (5 , 2 )), 0) AS \'completed_pct\',			 			
                COALESCE(CAST((SELECT ABS(AVG(all_passed_count.score)) FROM all_passed_count where id= class_id AND case_id =c.id AND score != \'0\') AS DECIMAL (5 , 2 )), 0) AS \'avg_score\',
               (SELECT  COUNT(*) FROM all_passed_count where id= class_id AND case_id =c.id AND score != \'0\' AND all_passed_count.passed LIKE \'%passed%\') AS \'passed\'
            FROM
                cases AS c
                	LEFT JOIN 
					 assignments AS assign ON assign.case_id = c.id
					 	LEFT JOIN 
					 classes ON assign.class_id = classes.id
					 	LEFT JOIN 
					 answers AS ans ON assign.id = ans.assignment_id AND ans.status = \'completed\'
                    LEFT JOIN
                service_lines AS s ON s.id = c.service_line_id
                    LEFT JOIN
                case_types AS ct ON ct.id = c.case_type_id
                    LEFT JOIN
                systems AS sys ON sys.id = c.system
            WHERE
            	 assign.class_id = class_id
                AND c.deleted_at IS NULL          
                GROUP BY c.id , c.case_type_id , c.service_line_id;
            
            END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
    }
}
