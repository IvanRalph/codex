<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('assignment_id');
            $table->unsignedBigInteger('class_pax_id');
            $table->string('status');
            $table->longText('answers')->nullable();
            $table->dateTime('last_saved')->nullable();
            $table->decimal('score',9,2)->default(0);
            $table->integer('poa_score')->nullable();
            $table->dateTime('last_computed')->nullable();
            $table->timestamps();

            $table->foreign('assignment_id')
                ->references('id')->on('assignments')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('class_pax_id')
                ->references('id')->on('class_paxes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}
