<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassedPercentageView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP VIEW IF EXISTS passed_percentage_view; CREATE VIEW `passed_percentage_view` AS
    SELECT 
        `cases`.`id` AS `id`,
        `cases`.`description` AS `description`,
        `cases`.`file` AS `file`,
        `cases`.`case_type_id` AS `case_type_id`,
        `cases`.`service_line_id` AS `service_line_id`,
        `cases`.`system` AS `system`,
        `cases`.`facility` AS `facility`,
        `cases`.`account` AS `account`,
        `cases`.`patient` AS `patient`,
        `cases`.`discharge` AS `discharge`,
        `cases`.`admission` AS `admission`,
        `cases`.`keyset` AS `keyset`,
        `cases`.`rationale` AS `rationale`,
        `cases`.`is_practice_case` AS `is_practice_case`,
        `cases`.`created_at` AS `created_at`,
        `cases`.`updated_at` AS `updated_at`,
        `cases`.`deleted_at` AS `deleted_at`,
        `cases`.`updated_by` AS `updated_by`
    FROM
        `cases`');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
