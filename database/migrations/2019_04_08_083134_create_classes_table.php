<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->dateTime('target_end');
            $table->integer('passing_rate')->default('80');
            $table->unsignedBigInteger('created_by');
            $table->integer('no_of_pax');
            $table->smallInteger('show_score')->default(0);
            $table->smallInteger('show_answer')->default(0);
            $table->smallInteger('show_rationale')->default(0);
            $table->smallInteger('show_prev_ans')->default(0);
            $table->string('status')->default('open');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('created_by')
            ->references('id')->on('users')
            ->onUpdate('cascade')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classes');
    }
}
