<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('class_id');
            $table->unsignedBigInteger('case_id');
            $table->dateTime('date_assigned')->nullable();
            $table->unsignedBigInteger('assigned_by');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('class_id')
            ->references('id')->on('classes')
            ->onUpdate('cascade')
            ->onDelete('cascade');

            $table->foreign('case_id')
                ->references('id')->on('cases')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('assigned_by')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
