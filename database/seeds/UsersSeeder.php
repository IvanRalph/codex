<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table('users')->insert([
            [
                'first_name'  => 'Ivan Ralph',
                'middle_name' => 'G',
                'last_name'   => 'Vitto',
                'username'    => 'IGVitto'
            ],
            [
                'first_name'  => 'James',
                'middle_name' => 'C',
                'last_name'   => 'Craig',
                'username'    => 'ECVitug'
            ],
            [
                'first_name'  => 'Evelyn',
                'middle_name' => 'M',
                'last_name'   => 'Tan',
                'username'    => 'EMTan'
            ],
            [
                'first_name'  => 'Peter',
                'middle_name' => 'A',
                'last_name'   => 'Edu',
                'username'    => 'PAEdu'
            ],
            [
                'first_name'  => 'Lacer Jake',
                'middle_name' => 'G',
                'last_name'   => 'Padilla',
                'username'    => 'LGPadilla'
            ],
            [
                'first_name'  => 'Mary Jane',
                'middle_name' => 'P',
                'last_name'   => 'Bombales',
                'username'    => 'MPBombales'
            ],
        ]);
    }
}
