<?php

use App\ServiceLine;
use Illuminate\Database\Seeder;

class ServiceLineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        ServiceLine::insert([
            [
                'description'  => 'Medical-Surgical',
                'case_type_id' => 1,
                'created_by'   => 1
            ],
            [
                'description'  => 'Psych',
                'case_type_id' => 1,
                'created_by'   => 1
            ],
            [
                'description'  => 'Rehab',
                'case_type_id' => 1,
                'created_by'   => 1
            ],
            [
                'description'  => 'OB',
                'case_type_id' => 1,
                'created_by'   => 1
            ],
            [
                'description'  => 'Newborn',
                'case_type_id' => 1,
                'created_by'   => 1
            ],
            [
                'description'  => 'SDS',
                'case_type_id' => 2,
                'created_by'   => 1
            ],
            [
                'description'  => 'ED',
                'case_type_id' => 2,
                'created_by'   => 1
            ],
            [
                'description'  => 'Observation',
                'case_type_id' => 2,
                'created_by'   => 1
            ],
            [
                'description'  => 'Ancillary',
                'case_type_id' => 2,
                'created_by'   => 1
            ],
            [
                'description'  => 'E&M',
                'case_type_id' => 2,
                'created_by'   => 1
            ],

        ]);
    }
}
