<?php

use App\System;
use Illuminate\Database\Seeder;

class SystemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        System::insert([
            [
                'description' => 'Integumentary',
                'created_by'  => 1
            ],
            [
                'description' => 'Musculoskeletal',
                'created_by'  => 1
            ],
            [
                'description' => 'Nervous',
                'created_by'  => 1
            ],
            [
                'description' => 'Circulatory',
                'created_by'  => 1
            ],
            [
                'description' => 'Lymphatic',
                'created_by'  => 1
            ],
            [
                'description' => 'Respiratory',
                'created_by'  => 1
            ],
            [
                'description' => 'Endocrine',
                'created_by'  => 1
            ],
            [
                'description' => 'Urinary/excretory',
                'created_by'  => 1
            ],
            [
                'description' => 'Reproductive',
                'created_by'  => 1
            ],
            [
                'description' => 'Digestive',
                'created_by'  => 1
            ],
        ]);
    }
}
