<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        DB::table('classes')->insert([
            'description'    => 'Test Description',
            'target_end'     => Carbon::now(),
            'created_by'     => 1,
            'no_of_pax'      => 5,
            'show_score'     => 0,
            'show_answer'    => 0,
            'show_rationale' => 0,
            'show_prev_ans'  => 0,
        ]);
    }
}
