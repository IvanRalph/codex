<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run ()
    {
        $this->call(UsersSeeder::class);
        $this->call(CaseTypeSeeder::class);
        $this->call(ServiceLineSeeder::class);
        $this->call(SystemsSeeder::class);
//        $this->call(ClassSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(PermissionsSeeder::class);
    }
}
