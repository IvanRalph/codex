<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        $perms = [
            //Case Maintenance
            [
                'name' => 'View Case Maintenance'
            ],
            [
                'name' => 'Add Case'
            ],
            [
                'name' => 'Edit Case'
            ],
            [
                'name' => 'Delete Case'
            ],
            [
                'name' => 'Export Cases to CSV'
            ],
            [
                'name' => 'Export Case Form to PDF'
            ],
            [
                'name' => 'View Cases Table'
            ],
            [
                'name' => 'Search Cases'
            ],
            //            Class Management
            [
                'name' => 'View Class Management'
            ],
            [
                'name' => 'Add Class'
            ],
            [
                'name' => 'Edit Class'
            ],
            [
                'name' => 'Delete Class'
            ],
            [
                'name' => 'Export Classes to CSV'
            ],
            [
                'name' => 'View Classes Table'
            ],
            [
                'name' => 'Search Classes'
            ],
            [
                'name' => 'Export Class form to PDF'
            ],
            [
                'name' => 'Assign Cases to Class'
            ],
            [
                'name' => 'Delete assigned Cases to Class'
            ],
            [
                'name' => 'Recalculate Scores'
            ],
            [
                'name' => 'View assigned Cases to Class'
            ],
            [
                'name' => 'Delete assigned Trainee to Class'
            ],
            [
                'name' => 'Assign Trainee to Class'
            ],
            [
                'name' => 'View assigned Trainee to Class'
            ],
            //            Training Module
            [
                'name' => 'View Work Queue'
            ],
            [
                'name' => 'Handle Cases'
            ],
            //            Settings - Roles
            [
                'name' => 'View Roles'
            ],
            [
                'name' => 'Assign Permissions to Roles'
            ],
            [
                'name' => 'View Users'
            ],
            [
                'name' => 'Add Users'
            ],
            [
                'name' => 'Edit Users'
            ],
            [
                'name' => 'Delete Users'
            ],
            [
                'name' => 'View Settings'
            ],
            //            Settings - References
            [
                'name' => 'View References'
            ],
            //            Settings - References - Case Types
            [
                'name' => 'View Case Types'
            ],
            [
                'name' => 'Add Case Types'
            ],
            [
                'name' => 'Edit Case Types'
            ],
            [
                'name' => 'Delete Case Types'
            ],
            //            Settings - References - Service Lines
            [
                'name' => 'View Service Lines'
            ],
            [
                'name' => 'Add Service Lines'
            ],
            [
                'name' => 'Edit Service Lines'
            ],
            [
                'name' => 'Delete Service Lines'
            ],
            //            Settings - References - Systems
            [
                'name' => 'View Systems'
            ],
            [
                'name' => 'Add Systems'
            ],
            [
                'name' => 'Edit Systems'
            ],
            [
                'name' => 'Delete Systems'
            ],
            [
                'name' => 'Replace Reference'
            ]
        ];

        foreach ($perms as $perm) {
            Permission::create($perm);
        }

        Role::find(1)->givePermissionTo(Permission::all());

        DB::table('model_has_roles')->insert([
            'role_id'    => '1',
            'model_type' => 'App\User',
            'model_id'   => '1'
        ]);
    }
}
