<?php

use App\CaseType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CaseTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CaseType::insert([
            [
                'description' => 'Inpatient',
                'with_poa' => 1,
                'created_by' => 1
            ],
            [
                'description' => 'Outpatient',
                'with_poa' => 0,
                'created_by' => 1
            ],
        ]);
    }
}
